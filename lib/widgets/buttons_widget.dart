import 'package:flutter/material.dart';

import '../utils/color_utils.dart';

class ButtonWidget extends StatelessWidget {
  final VoidCallback onTap;
  final Color color;
  final String title;
  final TextStyle? textStyle;
  final double width;
  final double height;
  final double? borderRadius;

  const ButtonWidget(
      {Key? key,
      required this.onTap,
      this.color = ColorUtils.primaryColor,
      required this.title,
      this.textStyle,
      this.width = double.infinity,
      this.height = 50,
      this.borderRadius})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).shadowColor,
              spreadRadius: 1,
              blurRadius: 5,
              offset: const Offset(3, 3.5),
            ),
          ],
          gradient: const LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color(0xff5F93E6),
                Color(0xff466CAA),
              ]),
          color: Colors.deepPurple.shade300,
          borderRadius: BorderRadius.circular(borderRadius ?? 8)),
      width: width,
      height: height,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.transparent),
          elevation: MaterialStateProperty.all(0),
        ),
        onPressed: onTap,
        child: Center(
          child: Text(
            title,
            style: textStyle ??
                const TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                ),
          ),
        ),
      ),
    );
  }
}
