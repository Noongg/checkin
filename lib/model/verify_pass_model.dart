class VerifyPass {
  bool isVerified;
  String label;

  VerifyPass({this.isVerified = false, required this.label});
}