class LanguageModel {
  String name;
  String icon;
  bool isSelected;

  LanguageModel(this.name, this.icon, this.isSelected);
}
