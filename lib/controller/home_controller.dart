import 'dart:async';
import 'package:check_in/helper/permission_helper.dart';
import 'package:check_in/helper/storage_helper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:permission_handler/permission_handler.dart';

import '../helper/firebase_helper.dart';
import '../routes/routes.dart';
import '../utils/strings.dart';

class HomeController extends GetxController with WidgetsBindingObserver {
  double? latitude;
  double? longitude;
  String? address;
  Set<Marker> marker = {};
  Completer<GoogleMapController> mapController = Completer();
  bool checkMap = false;
  String? imgPath;
  FirebaseAuth? _auth;
  String? nameUser;
  String? avatarUser;
  String? emailUser;
  double xOffset = 0;
  double yOffset = 0;
  double scaleFactor = 1;
  double xOffsetSecond = 0;
  double yOffsetSecond = 0;
  double scaleFactorSecond = 1;
  double xOffsetThird = 0;
  double yOffsetThird = 0;
  double scaleFactorThird = 1;
  double xOffsetFourth = 0;
  double yOffsetFourth = 0;
  double scaleFactorFourth = 1;
  bool isDrawerOpen = false;
  String? darkMapStyle;
  String? lightMapStyle;
  bool isCheckInternet = false;

  @override
  void onInit() {
    super.onInit();
    WidgetsBinding.instance!.addObserver(this);
    getProfile();
    perMissionLocation();
    _loadMapStyles();
    checkInternet();
  }

  @override
  void onClose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.onClose();
  }

  void checkInternet() async{
    isCheckInternet  = await InternetConnectionChecker().hasConnection;
    InternetConnectionChecker().onStatusChange.listen(
      (event) {
        isCheckInternet = event == InternetConnectionStatus.connected;
        update();
      },
    );
  }

  Future<void> _loadMapStyles() async {
    darkMapStyle = await rootBundle.loadString('assets/map_style/dark.json');
    lightMapStyle = await rootBundle.loadString('assets/map_style/light.json');
  }

  Future<void> setMapStyle() async {
    final controller = await mapController.future;
    bool isDarkMode = Get.isDarkMode;
    if (isDarkMode) {
      controller.setMapStyle(darkMapStyle);
    } else {
      controller.setMapStyle(lightMapStyle);
    }
    update();
  }

  void getProfile() {
    _auth = FirebaseAuth.instance;
    nameUser = _auth!.currentUser!.displayName;
    avatarUser = _auth!.currentUser!.photoURL;
    emailUser = _auth!.currentUser!.email;
    update();
  }

  void openDrawer() {
    xOffset = Get.height * 0.3;
    yOffset = Get.height * 0.08;
    scaleFactor = 0.8;
    xOffsetSecond = Get.height * 0.28;
    yOffsetSecond = Get.height * 0.11;
    scaleFactorSecond = 0.74;
    xOffsetThird = Get.height * 0.265;
    yOffsetThird = Get.height * 0.14;
    scaleFactorThird = 0.68;
    xOffsetFourth = Get.height * 0.252;
    yOffsetFourth = Get.height * 0.17;
    scaleFactorFourth = 0.62;
    isDrawerOpen = true;
    update();
  }

  void closeDrawer() {
    isDrawerOpen = false;
    xOffset = 0;
    yOffset = 0;
    scaleFactor = 1;
    xOffsetSecond = 0;
    yOffsetSecond = 0;
    scaleFactorSecond = 1;
    xOffsetThird = 0;
    yOffsetThird = 0;
    scaleFactorThird = 1;
    xOffsetFourth = 0;
    yOffsetFourth = 0;
    scaleFactorFourth = 1;
    update();
  }

  void checkIn() {
    PermissionHelper.requestPermission(
      onGranted: () {
        PermissionHelper.requestPermission(
            onGranted: () {
              getLocation();
              PermissionHelper.requestPermission(
                permission: Permission.microphone,
                onGranted: () {
                  if (isDrawerOpen) {
                    closeDrawer();
                  }
                  Get.toNamed(Routes.CAMERA_PAGE);
                },
              );
            },
            permission: Permission.location);
      },
    );
  }

  void perMissionLocation() async {
    await PermissionHelper.requestPermission(
        onGranted: () {
          getLocation();
        },
        permission: Permission.location);
  }

  void getLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    latitude = position.latitude;
    longitude = position.longitude;
    List<Placemark> placeMarks =
        await placemarkFromCoordinates(latitude!, longitude!);
    Placemark placeMark = placeMarks[0];
    address =
        '${placeMark.street}, ${placeMark.subAdministrativeArea}, ${placeMark.administrativeArea}';
    StorageHelper.box.write(StorageHelper.KEY_SAVE_LOCATION, address);
    marker.add(Marker(
        draggable: true,
        markerId: const MarkerId("id-1"),
        position: LatLng(latitude!, longitude!),
        infoWindow: InfoWindow(title: address)));
    checkMap = true;
    update();
  }
}
