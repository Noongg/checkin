import 'dart:io';
import 'package:camera/camera.dart';
import 'package:check_in/controller/home_controller.dart';
import 'package:check_in/controller/time_controller.dart';
import 'package:check_in/routes/routes.dart';
import 'package:check_in/widgets/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../helper/firebase_helper.dart';
import '../helper/storage_helper.dart';
import '../main.dart';
import '../utils/icons.dart';
import '../utils/strings.dart';

class ControllerCamera extends GetxController with WidgetsBindingObserver {
  CameraController? cameraController;
  int selectedCameraIndex = 1;
  bool initializing = false;
  String? imagePath;
  String checkTime = 'checkTime';
  String checkLocation = 'checkLocation';
  String? urlAvt;
  String? imgPath;
  String? name;
  File? imageFile;

  @override
  void onInit() async {
    // TODO: implement onInit
    super.onInit();
    WidgetsBinding.instance!.addObserver(this);
    initialize();
  }

  @override
  void onClose() {
    WidgetsBinding.instance!.removeObserver(this);
    cameraController!.dispose();
    super.onClose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (cameraController == null || !cameraController!.value.isInitialized) {
      return;
    }
    switch (state) {
      case AppLifecycleState.resumed:
        _setupCameraController(
          description: cameraController!.description,
        );
        break;
      case AppLifecycleState.inactive:
        cameraController!.dispose();
        break;
      case AppLifecycleState.paused:
        if (cameraController != null) {
          cameraController!.pausePreview();
        }
        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  void initialize() async {
    if (cameraController != null) return;
    cameraController = CameraController(listCameras[1], ResolutionPreset.veryHigh,
        imageFormatGroup: ImageFormatGroup.yuv420);
    await cameraController!.initialize();
    initializing = cameraController!.value.isInitialized;
    update();
  }

  Future _setupCameraController({
    required CameraDescription description,
  }) async {
    cameraController = CameraController(description, ResolutionPreset.veryHigh,
        enableAudio: false, imageFormatGroup: ImageFormatGroup.yuv420);
    await cameraController!.initialize();
    update();
  }

  void takePicture()async{
    XFile? imgPath = await cameraController!.takePicture();
    this.imgPath = imgPath.path;
    cameraController!.pausePreview();
    await uploadAvatar();
    timeKeeping();
    update();
  }

  void timeKeeping() async{
    TimeController timeController = Get.find();
    StorageHelper.box.write(StorageHelper.KEY_CHECK_IN, "true");
    StorageHelper.box
        .write(StorageHelper.KEY_SAVE_TIME, timeController.getHour);
    if (StorageHelper.box.read(StorageHelper.KEY_SAVE_CHECK_IN) == null) {
      String saveNumberCheckTime = checkTime + 1.toString();
      String saveNumberCheckLocation = checkLocation + 1.toString();
      String saveUrlAvt = 'urlAvt' + 1.toString();
      await FirebaseHelper.fireStore
          .collection("user")
          .doc(FirebaseHelper.auth.currentUser!.uid)
          .collection('timeKeeping')
          .doc(timeController.getDayMonthYear)
          .set({
        saveNumberCheckTime:timeController.getHour,
        saveUrlAvt:urlAvt,
        saveNumberCheckLocation:
        StorageHelper.box.read(StorageHelper.KEY_SAVE_LOCATION)
      }, SetOptions(merge: true)).then((_) {});
      StorageHelper.box.write(StorageHelper.KEY_SAVE_CHECK_IN, 1);
      Get.bottomSheet(
        Container(
          height: 380,
          width: Get.width,
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Column(
            children: [
              const Padding(padding: EdgeInsets.only(top: 50)),
              SvgPicture.asset(IconUtils.icSuccess),
              const Padding(padding: EdgeInsets.only(top: 10)),
              Text(
                StringUtils.timekeeping_success.tr,
                style: const TextStyle(
                    fontSize: 16, fontFamily: 'InterBold'),
              ),
              const Padding(padding: EdgeInsets.only(top: 100)),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xff0F296D),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  minimumSize:
                  Size(Get.width, 60),
                ),
                onPressed: () {
                  HomeController home =Get.find();
                  home.update();
                  Get.back();
                  Get.back();
                  Get.back();
                },
                child: Text(
                  StringUtils.close.tr,
                  style: const TextStyle(
                      fontSize: 14, fontFamily: 'InterBold'),
                ),
              )
            ],
          ),
        ),
        isDismissible: false,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16)),
        ),
      );
    }
    else {
      final numberSave =
      StorageHelper.box.read(StorageHelper.KEY_SAVE_CHECK_IN);
      int number = (numberSave + 1);
      String saveNumberCheckTime = checkTime + number.toString();
      String saveNumberCheckLocation = checkLocation + number.toString();
      String saveUrlAvt = 'urlAvt' + number.toString();
      await FirebaseHelper.fireStore
          .collection("user")
          .doc(FirebaseHelper.auth.currentUser!.uid)
          .collection('timeKeeping')
          .doc(timeController.getDayMonthYear)
          .set({
        saveNumberCheckTime: timeController.getHour,
        saveUrlAvt:urlAvt,
        saveNumberCheckLocation:
        StorageHelper.box.read(StorageHelper.KEY_SAVE_LOCATION)
      }, SetOptions(merge: true)).then((_) {});
      StorageHelper.box.write(StorageHelper.KEY_SAVE_CHECK_IN, number);
      Get.bottomSheet(
        Container(
          height: 380,
          width: Get.width,
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Column(
            children: [
              const Padding(padding: EdgeInsets.only(top: 50)),
              SvgPicture.asset(IconUtils.icSuccess),
              const Padding(padding: EdgeInsets.only(top: 10)),
              Text(
                StringUtils.timekeeping_success.tr,
                style: const TextStyle(
                    fontSize: 16, fontFamily: 'InterBold'),
              ),
              const Padding(padding: EdgeInsets.only(top: 100)),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xff0F296D),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  minimumSize:
                  Size(Get.width, 60),
                ),
                onPressed: () {
                  HomeController home =Get.find();
                  home.update();
                  Get.back();
                  Get.back();
                  Get.back();
                },
                child: Text(
                  StringUtils.close.tr,
                  style: const TextStyle(
                      fontSize: 14, fontFamily: 'InterBold'),
                ),
              )
            ],
          ),
        ),
        isDismissible: false,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16)),
        ),
      );
    }
    StorageHelper.box.remove(StorageHelper.KEY_TIME_KEEPING);
  }

  Future uploadAvatar() async {
    Get.dialog(const LoadingWidget());
    String imageName = FirebaseHelper.auth.currentUser!.uid;
    imageFile = File(imgPath!);
    name = FirebaseHelper.auth.currentUser!.displayName;
    try {
      final ref =
      FirebaseStorage.instance.ref().child('avatar/$imageName').child(DateTime.now().toString());
      TaskSnapshot taskSnapshot = await ref.putFile(imageFile!);
      urlAvt = await taskSnapshot.ref.getDownloadURL();
      await FirebaseHelper.auth.currentUser!.updatePhotoURL(urlAvt);
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
    }

  }
}
