import 'package:check_in/controller/home_controller.dart';
import 'package:check_in/repositories/auth_repository.dart';
import 'package:check_in/utils/icons.dart';
import 'package:check_in/utils/strings.dart';
import 'package:check_in/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class AbsentController extends GetxController {
  TimeLeave timeLeave = TimeLeave.day;
  bool isCheckDays = false;
  TextEditingController absentDayController = TextEditingController();
  TextEditingController reasonController = TextEditingController();
  TextEditingController reasonDaysController = TextEditingController();
  TextEditingController leaveFromController = TextEditingController();
  TextEditingController leaveToController = TextEditingController();
  List<String> listAbsentDay = [
    StringUtils.all_day.tr,
    StringUtils.morning.tr,
    StringUtils.afternoon.tr
  ];
  late String itemAbsentDay;
  bool isCheckSentMailOnLeaveOneDay = false;
  HomeController homeController = Get.find();

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    itemAbsentDay = listAbsentDay.first;
  }

  void changeTimeLeave() {
    switch (timeLeave) {
      case TimeLeave.day:
        timeLeave = TimeLeave.day;
        isCheckDays = false;
        break;
      case TimeLeave.days:
        timeLeave = TimeLeave.days;
        isCheckDays = true;
        break;
    }
    update();
  }

  void sentMailOnLeaveOneDay() async {
    Get.dialog(const LoadingWidget());
    String? name = homeController.nameUser;
    String reason = reasonController.text;
    String timeLeave = itemAbsentDay;
    String dayLeave = absentDayController.text;
    String? email = homeController.emailUser;
    if (reason.isEmpty || dayLeave.isEmpty) {
      if (dayLeave.isEmpty) {
        Get.snackbar(StringUtils.notification.tr,
            StringUtils.not_entered_the_holiday.tr);
      }
      if (reason.isEmpty) {
        Get.snackbar(
            StringUtils.notification.tr, StringUtils.not_entered_the_reason.tr);
      }
    } else {
      Map<String, dynamic> templateParams = {
        'from_name': name,
        'message': reason,
        'time_leave': timeLeave,
        'day_leave': dayLeave,
        'email': email
      };
      isCheckSentMailOnLeaveOneDay =
          await AuthRepository.postSentMail(templateParams, 'template_zpfsge9');
      update();
      if (isCheckSentMailOnLeaveOneDay) {
        Get.bottomSheet(
          Container(
            height: 380,
            width: Get.width,
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: Column(
              children: [
                const Padding(padding: EdgeInsets.only(top: 50)),
                SvgPicture.asset(IconUtils.icSuccess),
                const Padding(padding: EdgeInsets.only(top: 10)),
                Text(
                  StringUtils.email_sent_successfully.tr,
                  style: const TextStyle(fontSize: 16, fontFamily: 'InterBold'),
                ),
                const Padding(padding: EdgeInsets.only(top: 100)),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: const Color(0xff0F296D),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    minimumSize: Size(Get.width, 60),
                  ),
                  onPressed: () {
                    Get.back();
                    Get.back();
                    Get.back();
                    homeController.update();
                  },
                  child: Text(
                    StringUtils.close.tr,
                    style:
                        const TextStyle(fontSize: 14, fontFamily: 'InterBold'),
                  ),
                )
              ],
            ),
          ),
          isDismissible: false,
          backgroundColor: Colors.white,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16), topRight: Radius.circular(16)),
          ),
        );
      } else {
        Get.back();
        Get.snackbar(StringUtils.notification.tr, StringUtils.unsuccessful.tr);
      }
    }
  }

  void sentMailOnLeaveDays() async {
    Get.dialog(const LoadingWidget());
    String? name = homeController.nameUser;
    String? email = homeController.emailUser;
    String reason = reasonDaysController.text;
    String dayToLeave = leaveToController.text;
    String dayFromLeave = leaveFromController.text;
    if (reason.isEmpty || dayToLeave.isEmpty || dayFromLeave.isEmpty) {
      if (dayToLeave.isEmpty) {
        Get.snackbar(StringUtils.notification.tr,
            StringUtils.not_entered_the_holiday_from.tr);
      }
      if (dayFromLeave.isEmpty) {
        Get.snackbar(StringUtils.notification.tr,
            StringUtils.not_entered_the_holiday_to.tr);
      }
      if (reason.isEmpty) {
        Get.snackbar(
            StringUtils.notification.tr, StringUtils.not_entered_the_reason.tr);
      }
    } else {
      Map<String, dynamic> templateParams = {
        'from_name': name,
        'message': reason,
        'day_to_leave': dayToLeave,
        'day_from_leave': dayFromLeave,
        'email': email
      };
      isCheckSentMailOnLeaveOneDay =
          await AuthRepository.postSentMail(templateParams, 'template_nx7755q');
      update();
      if (isCheckSentMailOnLeaveOneDay) {
        Get.bottomSheet(
          Container(
            height: 380,
            width: Get.width,
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: Column(
              children: [
                const Padding(padding: EdgeInsets.only(top: 50)),
                SvgPicture.asset(IconUtils.icSuccess),
                const Padding(padding: EdgeInsets.only(top: 10)),
                Text(
                  StringUtils.email_sent_successfully.tr,
                  style: const TextStyle(fontSize: 16, fontFamily: 'InterBold'),
                ),
                const Padding(padding: EdgeInsets.only(top: 100)),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: const Color(0xff0F296D),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    minimumSize: Size(Get.width, 60),
                  ),
                  onPressed: () {
                    Get.back();
                    Get.back();
                    Get.back();
                    homeController.update();
                  },
                  child: Text(
                    StringUtils.close.tr,
                    style:
                        const TextStyle(fontSize: 14, fontFamily: 'InterBold'),
                  ),
                )
              ],
            ),
          ),
          isDismissible: false,
          backgroundColor: Colors.white,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16), topRight: Radius.circular(16)),
          ),
        );
      } else {
        Get.back();
        Get.snackbar(StringUtils.notification.tr, StringUtils.unsuccessful.tr);
      }
    }
  }
}

enum TimeLeave { day, days }
