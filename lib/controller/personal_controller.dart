import 'package:check_in/controller/home_controller.dart';
import 'package:check_in/controller/time_controller.dart';
import 'package:check_in/helper/firebase_helper.dart';
import 'package:check_in/model/chart_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class PersonalController extends GetxController {
  HomeController homeController = Get.find();
  TimeController timeController = Get.find();
  Map<String, dynamic>? dataTime;
  double? percentToday;
  double? percentMonth;
  String? timeWorkedToday;
  String? timeWorkedMonth;
  String? totalTimeWorkedMonth;
  int? totalMinutesCurrent;
  int? totalMinutesKeepingLast;
  int? totalMinutesKeepingDay;
  int totalMinutesKeepingMonth = 0;
  QueryDocumentSnapshot? queryDocumentSnapshot;
  QuerySnapshot? querySnapshot;
  bool isShowToday = false;
  bool isShowMonth = false;
  String? monthCurrent;
  List<Map<String, dynamic>> dataTimeKeepingOfMonth = [];
  Map<String, dynamic> mapChartData = {};
  List monthOfYearKeeping = [];
  final List<ChartModel> chartData = [];
  String? timeId;

  @override
  void onInit() {
    getPercentToday();
    getPercentMonth();
    super.onInit();
  }

  void getPercentToday() async {
    int? hoursCurrent = timeController.hours;
    int? minuteCurrent = timeController.minutes;
    queryDocumentSnapshot = await FirebaseHelper.getLastTimeKeeping();
    if (queryDocumentSnapshot != null) {
      queryDocumentSnapshot!.reference.id;
      if (timeController.timeZone == queryDocumentSnapshot!.reference.id) {
        dataTime = queryDocumentSnapshot!.data() as Map<String, dynamic>;
        update();
      }
    }
    if (dataTime == null) {
      percentToday = 0;
      timeWorkedToday = '00h:00m';
      isShowToday = true;
      update();
    } else {
      String timeKeeping = dataTime!['checkTime1'];
      int hoursKeeping = int.parse(timeKeeping.substring(0, 2));
      int minuteKeeping = int.parse(timeKeeping.substring(3, 5));
      int totalMinutesKeeping = hoursKeeping * 60 + minuteKeeping;
      List numbers = [];
      dataTime!.forEach(
        (key, value) {
          if(key.length >9){
            if (key.substring(9, 10).isNum) {
              numbers.add(key.substring(9, 10));
            }
          }
        },
      );
      numbers.sort();
      if (numbers.last == '1') {
        if (hoursCurrent! > 13 && hoursKeeping < 12) {
          totalMinutesCurrent = hoursCurrent * 60 + minuteCurrent! - 60;
        } else {
          totalMinutesCurrent = hoursCurrent * 60 + minuteCurrent!;
        }
        int totalMinutesWorked = totalMinutesCurrent! - totalMinutesKeeping;
        int hour = totalMinutesWorked ~/ 60;
        int minutes = totalMinutesWorked % 60;
        percentToday = totalMinutesWorked / 1440;
        timeWorkedToday =
            '${hour.toString().padLeft(2, "0")}h:${minutes.toString().padLeft(2, "0")}m';
        isShowToday = true;
        update();
      } else {
        String timeKeepingLast = dataTime!['checkTime${numbers.last}'];
        int hoursKeepingLast = int.parse(timeKeepingLast.substring(0, 2));
        int minuteKeepingLast = int.parse(timeKeepingLast.substring(3, 5));
        if (hoursKeepingLast > 13 && hoursKeeping < 12) {
          totalMinutesKeepingLast =
              hoursKeepingLast * 60 + minuteKeepingLast - 90;
        } else {
          totalMinutesKeepingLast = hoursKeepingLast * 60 + minuteKeepingLast;
        }
        int totalMinutesKeepingWorked =
            totalMinutesKeepingLast! - totalMinutesKeeping;
        int hour = totalMinutesKeepingWorked ~/ 60;
        int minutes = totalMinutesKeepingWorked % 60;
        percentToday = totalMinutesKeepingWorked / 1440;
        timeWorkedToday =
            '${hour.toString().padLeft(2, "0")}h:${minutes.toString().padLeft(2, "0")}m';
        isShowToday = true;
        update();
      }
    }
    update();
  }

  void getPercentMonth() async {
    querySnapshot = await FirebaseHelper.getAllTimeKeeping();
    monthCurrent = timeController.month;
    int dayOfMonth = minuteOfMonth(monthCurrent!);
    int totalMinutesWorked = dayOfMonth * 8 * 60;
    int totalHouseWorked = dayOfMonth * 8;
    totalTimeWorkedMonth = '$totalHouseWorked';

    if (querySnapshot == null) {
      percentMonth = 0;
      timeWorkedMonth = '0';
      isShowMonth = true;
      update();
    } else {
      String monthOfYearCurrent =
          '${timeController.year}-${timeController.month}';
      for (var element in querySnapshot!.docs) {
        timeId = element.reference.id;
        monthOfYearKeeping.add(timeId!.substring(0, 7));
        if (timeId!.substring(0, 7) == monthOfYearCurrent) {
          dataTimeKeepingOfMonth.add(element.data() as Map<String, dynamic>);
        }
        monthOfYearKeeping = monthOfYearKeeping.toSet().toList();

        for (var e in monthOfYearKeeping) {
          double totalMinutesKeepingManyMonth = 0;
          if (timeId!.substring(0, 7) == e) {
            int totalMinutesKeepingWorked = totalMinutesTimeKeepingMonth(
                element.data() as Map<String, dynamic>);
            totalMinutesKeepingManyMonth += totalMinutesKeepingWorked;
            chartData.add(
              ChartModel(
                e,
                double.parse(
                  (totalMinutesKeepingManyMonth / 60).toStringAsFixed(2),
                ),
              ),
            );
          }
        }
      }
      removeDuplicate(chartData);
      for (var e in dataTimeKeepingOfMonth) {
        int totalMinutesKeepingWorked = totalMinutesTimeKeepingMonth(e);
        totalMinutesKeepingMonth =
            totalMinutesKeepingMonth + totalMinutesKeepingWorked;
      }
      int hour = totalMinutesKeepingMonth ~/ 60;
      int minutes = totalMinutesKeepingMonth % 60;
      percentMonth = totalMinutesKeepingMonth / totalMinutesWorked;
      if (percentMonth! > 1) {
        percentMonth == 1;
      }
      timeWorkedMonth =
          '${hour.toString().padLeft(2, "0")}h:${minutes.toString().padLeft(2, "0")}m';
      isShowMonth = true;
      update();
    }
  }

  void removeDuplicate(List list){
    for (var i = 0; i < list.length - 1; i++) {
      if (list[i].time == list[i + 1].time) {
        var minutes = list[i].minutes + list[i + 1].minutes;
        list[i].minutes = minutes;
        list.removeAt(i + 1);
        removeDuplicate(list);
      }
    }
  }

  int totalMinutesTimeKeepingMonth(Map<String, dynamic> element) {
    List numbers = [];
    element.forEach(
      (key, value) {
        if(key.length>9){
          if (key.substring(9, 10).isNum) {
            numbers.add(key.substring(9, 10));
          }
        }
      },
    );
    numbers.sort();
    String timeKeeping = element['checkTime1'];
    int hoursKeeping = int.parse(timeKeeping.substring(0, 2));
    int minuteKeeping = int.parse(timeKeeping.substring(3, 5));
    int totalMinutesKeeping = hoursKeeping * 60 + minuteKeeping;
    String timeKeepingLast = element['checkTime${numbers.last}'];
    int hoursKeepingLast = int.parse(timeKeepingLast.substring(0, 2));
    int minuteKeepingLast = int.parse(timeKeepingLast.substring(3, 5));
    int totalMinutesTimeKeepingDay = 0;
    if (hoursKeepingLast > 13 && hoursKeeping < 12) {
      totalMinutesTimeKeepingDay =
          hoursKeepingLast * 60 + minuteKeepingLast - 60;
    } else {
      totalMinutesTimeKeepingDay = hoursKeepingLast * 60 + minuteKeepingLast;
    }
    int totalMinutesKeepingWorked =
        totalMinutesTimeKeepingDay - totalMinutesKeeping;
    return totalMinutesKeepingWorked;
  }

  int minuteOfMonth(String month) {
    switch (month) {
      case '1':
        return 31;
      case '2':
        bool isYear = checkYear(int.parse(timeController.year));
        return isYear ? 29 : 28;
      case '3':
        return 31;
      case '4':
        return 30;
      case '5':
        return 31;
      case '6':
        return 30;
      case '7':
        return 31;
      case '8':
        return 31;
      case '9':
        return 30;
      case '10':
        return 31;
      case '11':
        return 30;
      case '12':
        return 31;
      default:
        return 31;
    }
  }

  bool checkYear(int year) {
    if (year % 400 == 0) {
      return true;
    }
    if (year % 4 == 0 && year % 100 != 0) {
      return true;
    }
    return false;
  }
}
