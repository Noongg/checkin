import 'package:check_in/helper/firebase_helper.dart';
import 'package:check_in/helper/storage_helper.dart';
import 'package:check_in/utils/strings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import 'login_controller.dart';

class AuthController extends GetxController {
  static LoginController loginController = Get.find();

  bool isLogin = false;
  bool isCheckInternet = false;
  int? number;
  late ThemeMode themeMode;
  late Locale locale;

  @override
  void onInit() {
    super.onInit();
    getThemeMode();
    getLocale();
    checkInternet();
    FirebaseHelper.auth.authStateChanges().listen(
      (User? user) {
        if (user == null) {
          isLogin = false;
        } else {
          StorageHelper.box.erase();
          isLogin = true;
        }
        update();
      },
    );
  }

  void getThemeMode() {
    final isDarkMode =
        StorageHelper.boxTheme.read(StorageHelper.KEY_THEME_MODE);
    if (isDarkMode == true) {
      themeMode = ThemeMode.dark;
    } else if (isDarkMode == false) {
      themeMode = ThemeMode.light;
    } else if (isDarkMode == null) {
      themeMode = ThemeMode.system;
    }
    update();
  }

  void getLocale() {
    final language = StorageHelper.boxLanguage.read(StorageHelper.KEY_LANGUAGE);
    if (language == 'vi') {
      locale = const Locale('vi', 'VN');
    } else if (language == 'en') {
      locale = const Locale('en', 'US');
    } else if (language == null) {
      locale = const Locale('vi', 'VN');
    }
    update();
  }

  void checkInternet() {
    InternetConnectionChecker().onStatusChange.listen(
      (event) {
        isCheckInternet = event == InternetConnectionStatus.connected;
        if (isCheckInternet == false) {
          Get.snackbar(StringUtils.notification.tr,
              StringUtils.disconnection_internet.tr,backgroundColor: Colors.white,colorText: Colors.black);
        }
        update();
      },
    );
  }
}
