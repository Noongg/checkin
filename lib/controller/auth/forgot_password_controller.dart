import 'package:check_in/helper/firebase_helper.dart';
import 'package:check_in/routes/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../utils/strings.dart';

class ForgotPasswordController extends GetxController {
  TextEditingController forgotPasswordTextController = TextEditingController();
  FocusNode emailFocusNode = FocusNode();
  final emailKey = GlobalKey<FormFieldState>();


  String? validateEmail(String? value) {
    if (GetUtils.isNullOrBlank(value)!) {
      return StringUtils.not_enter_email_yet.tr;
    }
    if (!GetUtils.isEmail(value!)) {
      return StringUtils.email_invalid.tr;
    }
    return null;
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    checkFocusNode();
  }
  void checkFocusNode() {
    emailFocusNode.addListener(() {
      if (!emailFocusNode.hasFocus) {
        emailKey.currentState!.validate();
      }
    });
  }
  void sendMail() async{
    if (emailKey.currentState!.validate()) {
      await FirebaseHelper.auth.sendPasswordResetEmail(email: forgotPasswordTextController.text);
      forgotPasswordTextController.clear();
      Get.toNamed(Routes.EMAIL_SENT_PAGE);
    }
  }
}
