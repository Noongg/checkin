import 'package:check_in/helper/firebase_helper.dart';
import 'package:check_in/model/verify_pass_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../utils/strings.dart';
import '../../widgets/loading_widget.dart';

class RegisterController extends GetxController {
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  FocusNode fullNameFocusNode = FocusNode();
  FocusNode emailFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();
  FocusNode confirmPasswordFocusNode = FocusNode();
  final formKey = GlobalKey<FormState>();
  final fullNameKey = GlobalKey<FormFieldState>();
  final emailKey = GlobalKey<FormFieldState>();
  final passwordKey = GlobalKey<FormFieldState>();
  final confirmPasswordKey = GlobalKey<FormFieldState>();
  bool hidePassWord = true;
  bool hideConfirmPassWord = true;
  bool isCheckedAgreeTerm = false;
  bool isCheckedShowAgreeTerm = false;
  List<VerifyPass> listVerifyPass = [
    VerifyPass(label: StringUtils.lowercase_letters.tr),
    VerifyPass(label: StringUtils.digits.tr),
    VerifyPass(label: StringUtils.capital_letters.tr),
    VerifyPass(label: StringUtils.special_characters.tr),
    VerifyPass(label: StringUtils.eight_characters.tr),
  ];

  void firebaseSignUp() async {
    if (formKey.currentState!.validate() && isCheckedAgreeTerm) {
      Get.dialog(const LoadingWidget());
      await FirebaseHelper.registerFirebase(
          name: fullNameController.text,
          email: emailController.text,
          password: passwordController.text);
      fullNameController.clear();
      emailController.clear();
      passwordController.clear();
      confirmPasswordController.clear();
    }
  }

  void onChangedPass(String text) {
    if (text != text.toLowerCase()) {
      listVerifyPass[2].isVerified = true;
    } else {
      listVerifyPass[2].isVerified = false;
    }
    if (text != text.toUpperCase()) {
      listVerifyPass[0].isVerified = true;
    } else {
      listVerifyPass[0].isVerified = false;
    }
    if (text.contains(RegExp(r'[0-9]'))) {
      listVerifyPass[1].isVerified = true;
    } else {
      listVerifyPass[1].isVerified = false;
    }
    if (text.contains(RegExp(r'[!@#$%^&*]'))) {
      listVerifyPass[3].isVerified = true;
    } else {
      listVerifyPass[3].isVerified = false;
    }
    if (text.length >= 8) {
      listVerifyPass[4].isVerified = true;
    } else {
      listVerifyPass[4].isVerified = false;
    }
    update();
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    checkFocusNode();
  }

  void checkFocusNode() {
    fullNameFocusNode.addListener(() {
      if (!fullNameFocusNode.hasFocus) {
        fullNameKey.currentState!.validate();
      }
    });
    emailFocusNode.addListener(() {
      if (!emailFocusNode.hasFocus) {
        emailKey.currentState!.validate();
      }
    });
    passwordFocusNode.addListener(() {
      if (!passwordFocusNode.hasFocus) {
        passwordKey.currentState!.validate();
      }
    });
    confirmPasswordFocusNode.addListener(() {
      if (!confirmPasswordFocusNode.hasFocus) {
        confirmPasswordKey.currentState!.validate();
      }
    });
  }

  @override
  void dispose() {
    fullNameFocusNode.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    confirmPasswordFocusNode.dispose();
    fullNameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  void onClose() {
    fullNameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    fullNameFocusNode.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    confirmPasswordFocusNode.dispose();
    super.onClose();
  }

  void updateHidePassWord() {
    hidePassWord=!hidePassWord;
    update();
  }

  void updateHidConfirmPassWord() {
    hideConfirmPassWord=!hideConfirmPassWord;
    update();
  }

  void updateIsAgreeTerm() {
    isCheckedAgreeTerm = !isCheckedAgreeTerm;
    update();
  }

  String? validateConfirmPassword(String? value) {
    if (passwordController.text != confirmPasswordController.text) {
      return StringUtils.password_not_match.tr;
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (GetUtils.isNullOrBlank(value)!) {
      return StringUtils.not_enter_password_yet.tr;
    }
    for (var element in listVerifyPass) {
      if (!element.isVerified) {
        return StringUtils.password_invalid.tr;
      }
    }
    return null;
  }

  String? validateEmail(String? value) {
    if (GetUtils.isNullOrBlank(value)!) {
      return StringUtils.not_enter_email_yet.tr;
    }
    if (!GetUtils.isEmail(value!)) {
      return StringUtils.email_invalid.tr;
    }
    return null;
  }

  String? validateFullName(String? value) {
    if (GetUtils.isNullOrBlank(value)!) {
      return StringUtils.not_enter_full_name_yet.tr;
    }
    if (fullNameController.text.length < 10) {
      return StringUtils.full_name_invalid.tr;
    }
    return null;
  }
}


