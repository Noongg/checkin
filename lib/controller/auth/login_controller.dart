import 'package:check_in/helper/firebase_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/strings.dart';
import '../../widgets/loading_widget.dart';

class LoginController extends GetxController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  FocusNode emailFocusNode = FocusNode();
  final emailKey = GlobalKey<FormFieldState>();
  bool hidePassWord = true;
  bool isChecked = false;

  void firebaseLogin() async {
    if (formKey.currentState!.validate()) {
      Get.dialog(const LoadingWidget());
      await FirebaseHelper.loginFirebase(
          email: emailController.text, password: passwordController.text);
      emailController.clear();
      passwordController.clear();
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    checkFocusNode();
  }

  void checkFocusNode() {
    emailFocusNode.addListener(() {
      if (!emailFocusNode.hasFocus) {
        emailKey.currentState!.validate();
      }
    });
  }

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void updateHidePassWord() {
    hidePassWord = !hidePassWord;
    update();
  }

  void removeTextField() {
    formKey.currentState!.reset();
    emailController.clear();
    passwordController.clear();
  }

  void updateIsChecked() {
    if (isChecked == false) {
      isChecked = true;
    } else {
      isChecked = false;
    }
    update(['isChecked']);
  }

  String? validateEmail(String? value) {
    if (GetUtils.isNullOrBlank(value)!) {
      return StringUtils.not_enter_email_yet.tr;
    }
    if (!GetUtils.isEmail(value!)) {
      return StringUtils.email_invalid.tr;
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (GetUtils.isNullOrBlank(value)!) {
      return StringUtils.not_enter_password_yet.tr;
    }
    return null;
  }
}
