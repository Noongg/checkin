import 'package:check_in/controller/auth/auth_controller.dart';
import 'package:check_in/controller/time_controller.dart';
import 'package:check_in/helper/firebase_helper.dart';
import 'package:check_in/helper/storage_helper.dart';
import 'package:check_in/utils/color_utils.dart';
import 'package:check_in/utils/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_calendar/clean_calendar_event.dart';
import 'package:get/get.dart';

class TimeSheetsController extends GetxController {
  AuthController authController = Get.find();
  Locale? locale;
  int? lastCheckIn;
  QuerySnapshot? querySnapshot;
  final Map<DateTime, List<CleanCalendarEvent>> events = {};
  TimeController timeController = Get.find();
  int totalMinutesLast = 0;
  bool isShowCalendar = false;
  int totalDayOnLate = 0;
  int totalDayOnLeave = 0;
  int totalDayWork = 0;
  int totalDayWorkOfMonth = 0;

  List<String> listWeekDaysVi = ['T2', 'T3', 'T4', 'T5', 'T6', 'T7', 'CN'];
  List<String> listWeekDaysEn = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
  ];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getLocale();
    getAllTimeKeeping();
  }

  void getAllTimeKeeping() async {
    querySnapshot = await FirebaseHelper.getAllTimeKeeping();
    if (querySnapshot == null) {
    } else {
      for (var element in querySnapshot!.docs) {
        int year = int.parse(element.reference.id.substring(0, 4));
        int month = int.parse(element.reference.id.substring(5, 7));
        int day = int.parse(element.reference.id.substring(8, 10));
        Map<String, dynamic> dataCheckIn =
            element.data() as Map<String, dynamic>;
        String timeCheckInFirst = dataCheckIn['checkTime1'];
        int hoursFirst = int.parse(timeCheckInFirst.substring(0, 2));
        int minutesFirst = int.parse(timeCheckInFirst.substring(3, 5));
        int secondsFirst = int.parse(timeCheckInFirst.substring(6, 8));
        int totalMinutesFirst = hoursFirst * 60 + minutesFirst;
        int indexLastCheckIn = int.parse(getLastCheckIn(dataCheckIn));
        if (indexLastCheckIn > 1) {
          String timeCheckInLast = dataCheckIn['checkTime$indexLastCheckIn'];
          int hoursLast = int.parse(timeCheckInLast.substring(0, 2));
          int minutesLast = int.parse(timeCheckInLast.substring(3, 5));
          int secondsLast = int.parse(timeCheckInLast.substring(6, 8));
          if (hoursLast > 13 && hoursFirst < 12) {
            totalMinutesLast = hoursLast * 60 + minutesLast - 90;
            update();
          } else {
            totalMinutesLast = hoursLast * 60 + minutesLast;
            update();
          }
          int totalMinutesWork = totalMinutesLast - totalMinutesFirst;
          String localCheckInFirst = dataCheckIn['checkLocation1'];
          String localCheckInLast =
              dataCheckIn['checkLocation$indexLastCheckIn'];
          String eventTimeWork = '';
          String eventCheckOnLate = '';
          if (int.parse(timeController.month) == month) {
            totalDayWork = totalDayWork + 1;
            update();
          }
          if (totalMinutesWork > 480) {
            eventTimeWork = StringUtils.worked_enough_hours.tr;
            update();
          } else {
            eventTimeWork = StringUtils.not_worked_enough_hours.tr;
            update();
          }
          if (hoursFirst > 8) {
            eventCheckOnLate = StringUtils.be_late.tr;
            if (int.parse(timeController.month) == month) {
              totalDayOnLate = totalDayOnLate + 1;
            }
            update();
          } else {
            eventCheckOnLate = StringUtils.on_time.tr;
            update();
          }

          events.addAll(
            {
              DateTime(year, month, day): [
                CleanCalendarEvent('$eventCheckOnLate - $eventTimeWork',
                    startTime: DateTime(year, month, day, hoursFirst,
                        minutesFirst, secondsFirst),
                    endTime: DateTime(
                        year, month, day, hoursLast, minutesLast, secondsLast),
                    description: '$localCheckInFirst -\n$localCheckInLast',
                    isDone: totalMinutesWork > 480 || eventCheckOnLate == StringUtils.on_time.tr ? true : false,
                    color: totalMinutesWork > 480
                        ? ColorUtils.blueColor
                        : ColorUtils.errorColor),
              ],
            },
          );
        }
      }
      totalDayWorkOfMonth = totalWeekDayOfMonth(
          daysInMonth(
            DateTime(
              int.parse(timeController.year),
              int.parse(timeController.month),
            ),
          ),
          int.parse(timeController.year),
          int.parse(timeController.month),
          day: 1);
      int daysInCurrent = DateTimeRange(
        start: DateTime(
            int.parse(timeController.year), int.parse(timeController.month), 0),
        end: DateTime(
          int.parse(timeController.year),
          int.parse(timeController.month),
          int.parse(timeController.day)-1,
        ),
      ).duration.inDays;
      int totalDayInCurrent = totalWeekDayOfMonth(
        daysInCurrent,
        int.parse(timeController.year),
        int.parse(timeController.month),
      );
      totalDayOnLeave = totalDayInCurrent - totalDayWork;
    }
    Future.delayed(
      const Duration(seconds: 1),
      () {
        isShowCalendar = true;
        update();
      },
    );
    update();
  }

  String getLastCheckIn(Map<String, dynamic> element) {
    List numbers = [];
    element.forEach(
      (key, value) {
        if(key.length > 9){
          if (key.substring(9, 10).isNum) {
            numbers.add(key.substring(9, 10));
          }
        }
      },
    );
    numbers.sort();
    return numbers.last;
  }

  void getLocale() {
    final language = StorageHelper.boxLanguage.read(StorageHelper.KEY_LANGUAGE);
    if (language == 'vi') {
      locale = const Locale('vi', 'VN');
    } else if (language == 'en') {
      locale = const Locale('en', 'US');
    } else if (language == null) {
      locale = const Locale('vi', 'VN');
    }
    update();
  }

  int totalWeekDayOfMonth(int totalDayOfMonth, int year, int month,
      {int day = 1}) {
    int result = 0;
    DateTime tempDateTime = DateTime(year, month, day);
    for (int i = day; i <= totalDayOfMonth; i++) {
      tempDateTime = DateTime(tempDateTime.year, tempDateTime.month, i);
      if (tempDateTime.weekday == DateTime.saturday ||
          tempDateTime.weekday == DateTime.sunday) {
      } else {
        result++;
      }
    }
    return result;
  }

  int daysInMonth(DateTime date) {
    var firstDayThisMonth = DateTime(date.year, date.month, date.day);
    var firstDayNextMonth = DateTime(firstDayThisMonth.year,
        firstDayThisMonth.month + 1, firstDayThisMonth.day);
    return firstDayNextMonth.difference(firstDayThisMonth).inDays;
  }
}
