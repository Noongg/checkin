import 'package:check_in/controller/home_controller.dart';
import 'package:check_in/helper/firebase_helper.dart';
import 'package:check_in/helper/storage_helper.dart';
import 'package:check_in/model/verify_pass_model.dart';
import 'package:check_in/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../model/language_model.dart';
import '../utils/icons.dart';
import '../widgets/loading_widget.dart';

class SettingController extends GetxController
    with GetSingleTickerProviderStateMixin {
  HomeController homeController = Get.find();
  late CheckDarkMode checkDarkMode;
  TextEditingController passwordEditingController = TextEditingController();
  TextEditingController passwordOldEditingController = TextEditingController();
  TextEditingController passwordConfirmEditingController =
      TextEditingController();
  FocusNode passwordFocusNode = FocusNode();
  FocusNode passwordOldFocusNode = FocusNode();
  FocusNode confirmPasswordFocusNode = FocusNode();
  final passwordKey = GlobalKey<FormFieldState>();
  final passwordOldKey = GlobalKey<FormFieldState>();
  final confirmPasswordKey = GlobalKey<FormFieldState>();
  final formKey = GlobalKey<FormState>();
  bool hidePassWord = true;
  bool hideOldPassWord = true;
  bool hideConfirmPassWord = true;
  bool isShowButton = false;
  List<LanguageModel> listLanguage = [
    LanguageModel(StringUtils.english, IconUtils.icGreatBritain, false),
    LanguageModel(StringUtils.vietnamese, IconUtils.icVietnam, false)
  ];
  List<VerifyPass> listVerifyPass = [
    VerifyPass(label: StringUtils.lowercase_letters.tr),
    VerifyPass(label: StringUtils.digits.tr),
    VerifyPass(label: StringUtils.capital_letters.tr),
    VerifyPass(label: StringUtils.special_characters.tr),
    VerifyPass(label: StringUtils.eight_characters.tr),
  ];
  int? indexLanguage;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    darkModeState();
    languageState();
    checkFocusNode();
  }

  @override
  void onClose() {
    passwordEditingController.clear();
    passwordConfirmEditingController.clear();
    passwordOldEditingController.clear();
    passwordFocusNode.dispose();
    passwordOldFocusNode.dispose();
    confirmPasswordFocusNode.dispose();
    super.onClose();
  }

  @override
  void dispose() {
    passwordEditingController.dispose();
    passwordConfirmEditingController.dispose();
    passwordOldEditingController.dispose();
    super.dispose();
  }

  void changePassword() async{
    if (formKey.currentState!.validate()) {
      Get.dialog(const LoadingWidget());
      await FirebaseHelper.changePassword(passwordOldEditingController.text, passwordEditingController.text);
    }
  }

  void checkFocusNode() {
    passwordOldFocusNode.addListener(() {
      if (!passwordOldFocusNode.hasFocus) {
        passwordOldKey.currentState!.validate();
      }
    });
    passwordFocusNode.addListener(() {
      if (!passwordFocusNode.hasFocus) {
        passwordKey.currentState!.validate();
      }
    });
    confirmPasswordFocusNode.addListener(() {
      if (!confirmPasswordFocusNode.hasFocus) {
        confirmPasswordKey.currentState!.validate();
      }
    });
  }

  void updateHidePassWord() {
    hidePassWord = !hidePassWord;
    update();
  }

  void updateOldHidePassWord() {
    hideOldPassWord = !hideOldPassWord;
    update();
  }

  void updateHidConfirmPassWord() {
    hideConfirmPassWord = !hideConfirmPassWord;
    update();
  }

  void onChangedPass(String text) {
    if (text != text.toLowerCase()) {
      listVerifyPass[2].isVerified = true;
    } else {
      listVerifyPass[2].isVerified = false;
    }
    if (text != text.toUpperCase()) {
      listVerifyPass[0].isVerified = true;
    } else {
      listVerifyPass[0].isVerified = false;
    }
    if (text.contains(RegExp(r'[0-9]'))) {
      listVerifyPass[1].isVerified = true;
    } else {
      listVerifyPass[1].isVerified = false;
    }
    if (text.contains(RegExp(r'[!@#$%^&*]'))) {
      listVerifyPass[3].isVerified = true;
    } else {
      listVerifyPass[3].isVerified = false;
    }
    if (text.length >= 8) {
      listVerifyPass[4].isVerified = true;
    } else {
      listVerifyPass[4].isVerified = false;
    }
    update();
  }

  void darkModeState() {
    if (StorageHelper.boxTheme.read(StorageHelper.KEY_THEME_MODE) == true) {
      checkDarkMode = CheckDarkMode.on;
    } else if (StorageHelper.boxTheme.read(StorageHelper.KEY_THEME_MODE) ==
        false) {
      checkDarkMode = CheckDarkMode.off;
    } else {
      checkDarkMode = CheckDarkMode.none;
    }
    update();
  }

  void changeThemeMode() {
    switch (checkDarkMode) {
      case CheckDarkMode.on:
        checkDarkMode = CheckDarkMode.on;
        Get.changeThemeMode(ThemeMode.dark);
        StorageHelper.boxTheme.write(StorageHelper.KEY_THEME_MODE, true);
        break;
      case CheckDarkMode.off:
        checkDarkMode = CheckDarkMode.off;
        Get.changeThemeMode(ThemeMode.light);
        StorageHelper.boxTheme.write(StorageHelper.KEY_THEME_MODE, false);
        break;
      case CheckDarkMode.none:
        checkDarkMode = CheckDarkMode.none;
        Get.changeThemeMode(ThemeMode.system);
        StorageHelper.boxTheme.remove(StorageHelper.KEY_THEME_MODE);
        break;
    }
    Future.delayed(
      const Duration(milliseconds: 300),
      () async {
        await homeController.setMapStyle();
      },
    );
    update();
  }

  void languageState() {
    if (StorageHelper.boxLanguage.read(StorageHelper.KEY_LANGUAGE) == null) {
      indexLanguage = 1;
    } else if (StorageHelper.boxLanguage.read(StorageHelper.KEY_LANGUAGE) ==
        'vi') {
      indexLanguage = 1;
    } else {
      indexLanguage = 0;
    }
    update();
  }

  void changeLanguage(int index) {
    switch (index) {
      case 0:
        Get.updateLocale(const Locale('en', 'US'));
        StorageHelper.boxLanguage.write(StorageHelper.KEY_LANGUAGE, 'en');
        break;
      case 1:
        Get.updateLocale(const Locale('vi', 'VN'));
        StorageHelper.boxLanguage.write(StorageHelper.KEY_LANGUAGE, 'vi');
        break;
    }
    update();
  }

  String? validateConfirmPassword(String? value) {
    if (passwordEditingController.text !=
        passwordConfirmEditingController.text) {
      return StringUtils.password_not_match.tr;
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (GetUtils.isNullOrBlank(value)!) {
      return StringUtils.not_enter_password_yet.tr;
    }
    if(passwordEditingController.text == passwordOldEditingController.text){
      return StringUtils.password_match.tr;
    }
    for (var element in listVerifyPass) {
      if (!element.isVerified) {
        return StringUtils.password_invalid.tr;
      }
    }
    return null;
  }

  String? validateOldPassword(String? value) {
    if (GetUtils.isNullOrBlank(value)!) {
      return StringUtils.not_enter_password_yet.tr;
    }
    return null;
  }
}

enum CheckDarkMode { on, off, none }
