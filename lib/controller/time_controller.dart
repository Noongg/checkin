import 'dart:async';
import 'package:check_in/controller/home_controller.dart';
import 'package:check_in/model/time_model.dart';
import 'package:check_in/repositories/auth_repository.dart';
import 'package:check_in/utils/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import '../helper/firebase_helper.dart';
import '../helper/storage_helper.dart';

class TimeController extends GetxController {
  String getHour = '';
  String day = '';
  String month = '';
  String year = '';
  String getTime = '';
  String timeZone = '';
  int? hours;
  int? minutes;
  int? seconds;
  int weekdays = 1;
  TimeModel? timeModel;
  QueryDocumentSnapshot? queryDocumentSnapshot;
  List numbers = [];
  Map<String, dynamic>? dataTime;
  bool isCheckInternet = false;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    checkInternet();
  }

  void checkInternet() async{
    isCheckInternet  = await InternetConnectionChecker().hasConnection;
    if ( isCheckInternet == false) {
      getHour = '';
      timeModel = TimeModel();
    } else {
      getTimeApi();
    }
    InternetConnectionChecker().onStatusChange.listen(
      (event) {
        isCheckInternet = event == InternetConnectionStatus.connected;
        if (isCheckInternet == false) {
          getHour = '';
          timeModel = TimeModel();
        } else {
          getTimeApi();
        }
        update();
      },
    );
  }

  void getTimeApi() async {
    timeModel = await AuthRepository.getTimeZone();
    if (timeModel == null) {
      return;
    } else {
      getTimeKeeping();
      timeZone = timeModel!.datetime!.substring(0, 10);
      day = timeZone.substring(8, 10);
      month = timeZone.substring(5, 7);
      year = timeZone.substring(0, 4);
      weekdays = timeModel!.dayOfWeek!;
      getDate();
    }
    update();
    HomeController homeController = Get.find();
    homeController.update();
  }

  void getTimeKeeping() async {
    queryDocumentSnapshot = await FirebaseHelper.getLastTimeKeeping();
    if (queryDocumentSnapshot != null) {
      queryDocumentSnapshot!.reference.id;
      if (timeZone == queryDocumentSnapshot!.reference.id) {
        StorageHelper.box.write(StorageHelper.KEY_CHECK_IN, "true");
        dataTime = queryDocumentSnapshot!.data() as Map<String, dynamic>;
        dataTime!.forEach(
          (key, value) {
            if (key.substring(9, 10).isNum) {
              numbers.add(key.substring(9, 10));
            }
          },
        );
        numbers.sort();
        if (numbers.last != null) {
          int number = int.parse(numbers.last);
          StorageHelper.box.write(StorageHelper.KEY_SAVE_CHECK_IN, number);
          StorageHelper.box.write(StorageHelper.KEY_SAVE_TIME,
              dataTime!['checkTime${numbers.last}']);
        }
      }
    } else {
      StorageHelper.box.erase();
    }
    HomeController homeController = Get.find();
    homeController.update();
    update();
  }

  get getDayOfWeek {
    return getTime = '$dayOfWeek, '
        '${StringUtils.day.tr} $day, '
        '${StringUtils.month.tr} $month, '
        '${StringUtils.year.tr} $year';
  }

  get dayOfWeek {
    switch (weekdays) {
      case 1:
        return StringUtils.monday.tr;
      case 2:
        return StringUtils.tuesday.tr;
      case 3:
        return StringUtils.wednesday.tr;
      case 4:
        return StringUtils.thursday.tr;
      case 5:
        return StringUtils.friday.tr;
      case 6:
        return StringUtils.friday.tr;
      case 7:
        return StringUtils.sunday.tr;
      default:
        return StringUtils.error.tr;
    }
  }

  void getDate() {
    hours = int.parse(timeModel!.datetime!.substring(11, 13));
    minutes = int.parse(timeModel!.datetime!.substring(14, 16));
    seconds = int.parse(timeModel!.datetime!.substring(17, 19));
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    Timer.periodic(
      const Duration(seconds: 1),
      (Timer t) async {
        if (isCheckInternet == false) {
          t.cancel();
        }
        seconds = seconds! + 1;
        final hour = twoDigits(hours!.remainder(24));
        final minute = twoDigits(minutes!.remainder(60));
        final second = twoDigits(seconds!.remainder(60));
        if (second == "59") {
          minutes = minutes! + 1;
        }
        if (minute == "59" && second == "59") {
          hours = hours! + 1;
        }
        getHour = "$hour:$minute:$second";
        update(["checkTime"]);
      },
    );
  }

  get getDayMonthYear {
    return '$year-$month-$day';
  }
}
