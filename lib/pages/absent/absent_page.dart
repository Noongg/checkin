import 'package:check_in/controller/absent_controller.dart';
import 'package:check_in/utils/color_utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/strings.dart';
import '../../widgets/buttons_widget.dart';

class AbsentPage extends GetView<AbsentController> {
  const AbsentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Material(
              color: ColorUtils.primaryColor,
              child: Row(
                children: [
                  IconButton(
                    splashRadius: 20,
                    padding: const EdgeInsets.only(left: 10),
                    onPressed: () {
                      Get.back();
                    },
                    icon: const Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Text(
                      StringUtils.on_leave.tr,
                      style: const TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
            Flexible(child: onLeaveWidget()),
          ],
        ),
      ),
    );
  }

  Widget onLeaveWidget() {
    return SingleChildScrollView(
      child: GetBuilder<AbsentController>(
        init: AbsentController(),
        builder: (AbsentController absentController) {
          return Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      StringUtils.time.tr,
                      style: const TextStyle(
                          fontSize: 12, fontWeight: FontWeight.w800),
                    ),
                    Row(
                      children: [
                        Radio<TimeLeave>(
                          fillColor: MaterialStateColor.resolveWith(
                              (states) => ColorUtils.primaryColor),
                          focusColor: MaterialStateColor.resolveWith(
                              (states) => ColorUtils.primaryColor),
                          value: TimeLeave.day,
                          groupValue: absentController.timeLeave,
                          onChanged: (TimeLeave? value) {
                            absentController.timeLeave = value!;
                            absentController.changeTimeLeave();
                          },
                        ),
                        Text(StringUtils.one_day.tr)
                      ],
                    ),
                    Row(
                      children: [
                        Radio<TimeLeave>(
                          fillColor: MaterialStateColor.resolveWith(
                              (states) => ColorUtils.primaryColor),
                          focusColor: MaterialStateColor.resolveWith(
                              (states) => ColorUtils.primaryColor),
                          value: TimeLeave.days,
                          groupValue: absentController.timeLeave,
                          onChanged: (TimeLeave? value) {
                            absentController.timeLeave = value!;
                            absentController.changeTimeLeave();
                          },
                        ),
                        Text(StringUtils.days.tr)
                      ],
                    ),
                  ],
                ),
                absentController.isCheckDays
                    ? Column(
                        children: [
                          timeLeave(StringUtils.break_from.tr,
                              absentController.leaveFromController),
                          const SizedBox(
                            height: 10,
                          ),
                          timeLeave(StringUtils.break_to.tr,
                              absentController.leaveToController),
                          const SizedBox(
                            height: 18,
                          ),
                          reasonTimeLeave(
                            StringUtils.reason.tr,
                            absentController.reasonDaysController,
                            () {
                              absentController.sentMailOnLeaveDays();
                            },
                          ),
                        ],
                      )
                    : Column(
                        children: [
                          timeLeave(StringUtils.day_off.tr,
                              absentController.absentDayController),
                          dropDownTimeLeave(
                            StringUtils.the_break.tr,
                            absentController.itemAbsentDay,
                            absentController.listAbsentDay,
                            (String? value) {
                              absentController.itemAbsentDay = value!;
                              absentController.update();
                            },
                          ),
                          reasonTimeLeave(
                            StringUtils.reason.tr,
                            absentController.reasonController,
                            () {
                              absentController.sentMailOnLeaveOneDay();
                            },
                          ),
                        ],
                      ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget timeLeave(String title, TextEditingController textEditingController) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            title,
            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w800),
          ),
          SizedBox(
            width: 220,
            height: 30,
            child: TextField(
              textAlign: TextAlign.right,
              controller: textEditingController,
              maxLines: 1,
              keyboardType: TextInputType.datetime,
              decoration: const InputDecoration(
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey, width: 0.6)),
                hintText: "01/01/2022",
                hintStyle: TextStyle(
                    fontSize: 14,
                    fontFamily: "InterLight",
                    fontWeight: FontWeight.w200),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget dropDownTimeLeave(String title, String value, List<String> list,
      Function(String?)? onChanged) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(
              title,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
            ),
          ),
          SizedBox(
            width: 220,
            child: DropdownButton(
              value: value,
              items: list
                  .map(
                    (e) => DropdownMenuItem(
                      value: e,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          e,
                          style: const TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
                  )
                  .toList(),
              isExpanded: true,
              underline: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 0.4),
                ),
              ),
              onChanged: onChanged,
            ),
          ),
        ],
      ),
    );
  }

  Widget reasonTimeLeave(String title,
      TextEditingController textEditingController, VoidCallback callback) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 20, top: 15),
          child: Text(
            title,
            style: const TextStyle(fontSize: 12, fontWeight: FontWeight.w800),
          ),
        ),
        TextField(
          controller: textEditingController,
          maxLines: 8,
          decoration: InputDecoration(
            hintText: StringUtils.car_failure.tr,
            hintStyle: const TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w300,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        ButtonWidget(
          height: 40,
          title: StringUtils.send.tr,
          onTap: callback,
        ),
      ],
    );
  }
}
