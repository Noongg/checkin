import 'package:check_in/controller/time_sheets_controller.dart';
import 'package:check_in/utils/color_utils.dart';
import 'package:check_in/utils/strings.dart';
import 'package:check_in/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_calendar/flutter_clean_calendar.dart';
import 'package:get/get.dart';

class TimeSheets extends GetView<TimeSheetsController> {
  const TimeSheets({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: SizedBox(
            height: Get.height,
            width: Get.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Material(
                  color: ColorUtils.primaryColor,
                  child: Row(
                    children: [
                      IconButton(
                        splashRadius: 20,
                        padding: const EdgeInsets.only(left: 10),
                        onPressed: () {
                          Get.back();
                        },
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text(
                          StringUtils.time_sheets.tr,
                          style: const TextStyle(
                              fontSize: 22, color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Text(
                    'Tháng này:',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                workDay(context),
                const Divider(
                  color: Colors.grey,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 10, bottom: 20),
                  child: Text(
                    '${StringUtils.time_sheets.tr}:',
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                timekeepingSchedule(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget workDay(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: GetBuilder<TimeSheetsController>(
          init: TimeSheetsController(),
          builder: (TimeSheetsController timeSheetsController) {
            return Container(
              height: 80,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                      color: Theme.of(context).shadowColor,
                      offset: const Offset(0, 4),
                      blurRadius: 8,
                      spreadRadius: 1)
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${timeSheetsController.totalDayWork}/${timeSheetsController.totalDayWorkOfMonth}',
                        style: const TextStyle(
                            color: ColorUtils.blueColor,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        StringUtils.workday.tr,
                        style: TextStyle(
                            color: Theme.of(context).textTheme.bodySmall!.color,
                            fontSize: 16),
                      ),
                    ],
                  ),
                  const Icon(
                    Icons.more_vert,
                    color: ColorUtils.shade60Color,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${timeSheetsController.totalDayOnLate}',
                        style: const TextStyle(
                            color: ColorUtils.errorColor,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        StringUtils.be_late.tr,
                        style: TextStyle(
                            color: Theme.of(context).textTheme.bodySmall!.color,
                            fontSize: 16),
                      ),
                    ],
                  ),
                  const Icon(
                    Icons.more_vert,
                    color: ColorUtils.shade60Color,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                       Text(
                         '${timeSheetsController.totalDayOnLeave}',
                        style:const TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        StringUtils.off_work.tr,
                        style: TextStyle(
                            color: Theme.of(context).textTheme.bodySmall!.color,
                            fontSize: 16),
                      ),
                    ],
                  ),
                  const SizedBox(),
                ],
              ),
            );
          }),
    );
  }

  Widget timekeepingSchedule(BuildContext context) {
    return GetBuilder<TimeSheetsController>(
      init: TimeSheetsController(),
      builder: (TimeSheetsController timeSheetsController) {
        return Flexible(
          fit: FlexFit.loose,
          child: timeSheetsController.isShowCalendar
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Calendar(
                    startOnMonday: true,
                    events: timeSheetsController.events,
                    isExpandable: true,
                    hideArrows: true,
                    eventDoneColor: ColorUtils.blueColor,
                    selectedColor: ColorUtils.primaryColor,
                    todayColor: ColorUtils.successColor,
                    bottomBarColor: Colors.grey.shade200,
                    eventColor: ColorUtils.errorColor,
                    locale: timeSheetsController.locale!.languageCode,
                    weekDays: timeSheetsController.locale!.languageCode == 'vi'
                        ? controller.listWeekDaysVi
                        : controller.listWeekDaysEn,
                    hideTodayIcon: true,
                    isExpanded: true,
                    expandableDateFormat: 'EEEE, dd MMMM yyyy',
                    dayOfWeekStyle: TextStyle(
                        color: Theme.of(context).textTheme.bodySmall!.color,
                        fontWeight: FontWeight.w800,
                        fontSize: 11),
                  ),
                )
              : Dialog(
                  backgroundColor: Theme.of(context).primaryColor,
                  insetPadding: EdgeInsets.zero,
                  child: const LoadingWidget(),
                ),
        );
      },
    );
  }
}
