import 'package:check_in/utils/color_utils.dart';
import 'package:check_in/utils/icons.dart';
import 'package:check_in/utils/strings.dart';
import 'package:check_in/widgets/buttons_widget.dart';
import 'package:check_in/widgets/text_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controller/auth/forgot_password_controller.dart';

class ForgotPasswordPage extends GetWidget<ForgotPasswordController> {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        backgroundColor: ColorUtils.primaryColor,
        title: Text(StringUtils.forgot_password.tr),
        elevation: 0,
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: Get.height * 0.8,
            width: Get.width,
            padding: const EdgeInsets.only(left: 32, right: 32),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: Get.height * 0.35,
                  child: Image.asset(
                    IconUtils.icForgot,
                    height: Get.height * 0.3,
                    alignment: Alignment.topCenter,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  StringUtils.open_mail.tr,
                  style: const TextStyle(
                      fontSize: 13,
                      color: ColorUtils.shade60Color,
                      height: 1.3),
                ),
                const SizedBox(
                  height: 30,
                ),
                Flexible(
                  child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        children: [
                          TextFieldWidget(
                            globalKey: controller.emailKey,
                            focusNode: controller.emailFocusNode,
                            controller: controller.forgotPasswordTextController,
                            title: StringUtils.email.tr,
                            hintText: "deniel123@gmail.com",
                            isRequired: true,
                            validator: controller.validateEmail,
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          ButtonWidget(
                              height: 40,
                              onTap: () {
                                controller.sendMail();
                              },
                              title: StringUtils.send.tr),
                        ],
                      )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
