import 'package:check_in/utils/icons.dart';
import 'package:check_in/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../routes/routes.dart';
import '../../widgets/buttons_widget.dart';

class SuccessPage extends StatelessWidget {
  const SuccessPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                SizedBox(
                  height: Get.height * 0.3,
                ),
                SvgPicture.asset(IconUtils.icComplete),
                const SizedBox(
                  height: 32,
                ),
                Text(
                  StringUtils.congratulations.tr,
                  style:  TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).textTheme.bodyMedium!.color,
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  StringUtils.successfully_registered.tr,
                  style:  TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: Theme.of(context).textTheme.bodyMedium!.color),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ButtonWidget(
              onTap: () {
                Get.offAllNamed(Routes.HOMEPAGE);
              },
              title: StringUtils.continue_key.tr,
              borderRadius: 0,
            ),
          )
        ],
      ),
    );
  }
}
