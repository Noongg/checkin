import 'package:check_in/controller/auth/forgot_password_controller.dart';
import 'package:check_in/controller/auth/login_controller.dart';
import 'package:check_in/routes/routes.dart';
import 'package:check_in/utils/icons.dart';
import 'package:check_in/utils/strings.dart';
import 'package:check_in/widgets/buttons_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class EmailResetPasswordPage extends StatelessWidget {
  const EmailResetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Container(
          height: Get.height * 0.8,
          width: Get.width,
          padding: const EdgeInsets.only(left: 32, right: 32),
          child: Column(
            children: [
              const SizedBox(
                height: 70,
              ),
              Text(
                StringUtils.email_has_sent.tr,
                style:  TextStyle(color: Theme.of(context).textTheme.bodyMedium!.color,
                    fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 100,
              ),
              SvgPicture.asset(IconUtils.icOpenEmail, height: 200),
              const SizedBox(
                height: 50,
              ),
              Text(
                StringUtils.check_email.tr,
                textAlign: TextAlign.center,
                style:  TextStyle(fontSize: 16, color: Theme.of(context).textTheme.bodyMedium!.color),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                StringUtils.have_sent_recover_pass.tr,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 13, color: Theme.of(context).textTheme.bodyMedium!.color, height: 1.3),
              ),
              const SizedBox(
                height: 70,
              ),
              Flexible(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: ButtonWidget(
                      height: 40,
                      onTap: () {
                        Get.delete<LoginController>();
                        Get.delete<ForgotPasswordController>();
                        Get.offAllNamed(Routes.LOGIN_PAGE);
                      },
                      title: StringUtils.back_to_login.tr),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
