import 'package:check_in/controller/personal_controller.dart';
import 'package:check_in/model/chart_model.dart';
import 'package:check_in/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../utils/color_utils.dart';
import '../../utils/strings.dart';

class PersonalPage extends GetWidget<PersonalController> {
  const PersonalPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  color: ColorUtils.primaryColor,
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Material(
                      color: ColorUtils.primaryColor,
                      child: IconButton(
                        splashRadius: 20,
                        padding: const EdgeInsets.only(left: 10),
                        onPressed: () {
                          Get.back();
                        },
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Stack(
                  alignment: Alignment.bottomCenter,
                  clipBehavior: Clip.none,
                  children: [
                    Container(
                      color: ColorUtils.primaryColor,
                      height: Get.height * 0.13,
                    ),
                    Positioned(
                      bottom: -40,
                      child: Container(
                        height: 140,
                        width: 140,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Theme.of(context).primaryColor),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            controller.homeController.avatarUser == null ||
                                    controller.homeController.isCheckInternet ==
                                        false
                                ? const CircleAvatar()
                                : ClipOval(
                                    child: FadeInImage.assetNetwork(
                                        height: 130,
                                        width: 130,
                                        placeholder:
                                            'assets/images/loading.gif',
                                        image: controller
                                            .homeController.avatarUser!,
                                        fit: BoxFit.cover),
                                  ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 50,
                ),
                Text(
                  controller.homeController.nameUser!,
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Theme.of(context).textTheme.bodySmall!.color,
                      height: 1.6,
                      fontSize: 30),
                ),
                Text(
                  controller.homeController.emailUser!,
                  style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      color: ColorUtils.shade60Color,
                      height: 1.6,
                      fontSize: 14),
                ),
                const SizedBox(
                  height: 30,
                ),
                GetBuilder<PersonalController>(
                  init: PersonalController(),
                  builder: (PersonalController personalController) {
                    return Container(
                      height: 350,
                      width: Get.width * 0.9,
                      decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        boxShadow: [
                          BoxShadow(
                            color: Theme.of(context).shadowColor,
                            blurRadius: 10,
                            spreadRadius: 3,
                            offset: const Offset(0, 3),
                          )
                        ],
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        children: [
                          TabBar(
                            unselectedLabelColor: ColorUtils.shade60Color,
                            tabs: [
                              Tab(text: StringUtils.today.tr),
                              Tab(text: StringUtils.this_month.tr)
                            ],
                          ),
                          Flexible(
                            child: TabBarView(
                              children: [
                                personalController.isShowToday
                                    ? CircularPercentIndicator(
                                        radius: 100,
                                        animation: true,
                                        animationDuration: 1200,
                                        lineWidth: 20,
                                        percent:
                                            personalController.percentToday!,
                                        center: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              StringUtils.have_worked.tr,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      .bodySmall!
                                                      .color),
                                            ),
                                            Text(
                                              personalController
                                                  .timeWorkedToday!,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 28,
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyMedium!
                                                    .color,
                                              ),
                                            ),
                                          ],
                                        ),
                                        circularStrokeCap:
                                            CircularStrokeCap.square,
                                        progressColor:
                                            controller.percentToday! >= 8 / 24
                                                ? ColorUtils.primaryColor
                                                : ColorUtils.errorColor,
                                      )
                                    : const LoadingWidget(),
                                personalController.isShowMonth
                                    ? CircularPercentIndicator(
                                        radius: 100,
                                        animation: true,
                                        animationDuration: 1200,
                                        lineWidth: 20,
                                        percent: controller.percentMonth!,
                                        center: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              StringUtils.have_worked.tr,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      .bodySmall!
                                                      .color),
                                            ),
                                            Text(
                                              controller.timeWorkedMonth!,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 28,
                                                color: Theme.of(context)
                                                    .textTheme
                                                    .bodyMedium!
                                                    .color,
                                              ),
                                            ),
                                            Text(
                                              '${StringUtils.of.tr} ${controller.totalTimeWorkedMonth!}h',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Theme.of(context)
                                                      .textTheme
                                                      .bodySmall!
                                                      .color),
                                            ),
                                          ],
                                        ),
                                        circularStrokeCap:
                                            CircularStrokeCap.square,
                                        progressColor:
                                            controller.percentMonth == 1
                                                ? ColorUtils.primaryColor
                                                : ColorUtils.errorColor,
                                      )
                                    : const LoadingWidget(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
                GetBuilder<PersonalController>(
                  init: PersonalController(),
                  builder: (PersonalController personalController) {
                    return personalController.isShowMonth
                        ? Container(
                            padding: const EdgeInsets.only(top: 20, right: 20),
                            height: 350,
                            width: Get.width * 0.9,
                            decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                              boxShadow: [
                                BoxShadow(
                                  color: Theme.of(context).shadowColor,
                                  blurRadius: 10,
                                  spreadRadius: 3,
                                  offset: const Offset(0, 3),
                                )
                              ],
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: SfCartesianChart(
                              title: ChartTitle(
                                text: StringUtils.time_chart_of_month.tr,
                                textStyle: const TextStyle(fontSize: 12),
                                alignment: ChartAlignment.far,
                              ),
                              primaryXAxis: CategoryAxis(
                                isVisible: true,
                                interval: 0.5,
                                title: AxisTitle(
                                    text: StringUtils.month.tr,
                                    alignment: ChartAlignment.far),
                              ),
                              primaryYAxis: NumericAxis(
                                  title: AxisTitle(
                                      text: StringUtils.hours.tr,
                                      alignment: ChartAlignment.far),
                                  isVisible: true),
                              tooltipBehavior: TooltipBehavior(enable: true),
                              series: [
                                LineSeries<ChartModel, String>(
                                    name: StringUtils.hours.tr,
                                    dataSource: personalController.chartData,
                                    dataLabelSettings: const DataLabelSettings(
                                      isVisible: true,
                                    ),
                                    enableTooltip: true,
                                    xValueMapper: (ChartModel data, _) =>
                                        data.time,
                                    yValueMapper: (ChartModel data, _) =>
                                        data.minutes)
                              ],
                            ),
                          )
                        : const LoadingWidget();
                  },
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
