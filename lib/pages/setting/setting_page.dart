import 'package:check_in/controller/setting_controller.dart';
import 'package:check_in/routes/routes.dart';
import 'package:check_in/utils/color_utils.dart';
import 'package:check_in/utils/icons.dart';
import 'package:check_in/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class SettingPage extends GetWidget<SettingController> {
  const SettingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.primaryColor,
      body: SafeArea(
        child: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                IconButton(
                  splashRadius: 20,
                  padding: const EdgeInsets.only(left: 10),
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                ),
                Flexible(
                  child: Center(
                    child: Text(
                      StringUtils.setting.tr,
                      style: const TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 48,
                  height: 48,
                )
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(35),
                        topRight: Radius.circular(35))),
                padding: const EdgeInsets.only(top: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    itemSetting(
                      context,
                      Icons.dark_mode,
                      StringUtils.dark_mode.tr,
                      () {
                        Get.toNamed(Routes.DARK_MODE_PAGE);
                      },
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    itemSetting(
                      context,
                      Icons.password,
                      StringUtils.change_password.tr,
                      () {
                        Get.toNamed(Routes.CHANGE_PASSWORD_PAGE);
                      },
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    itemSetting(
                      context,
                      Icons.language,
                      StringUtils.language.tr,
                      () {
                        Get.toNamed(Routes.LANGUAGE_PAGE);
                      },
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    itemSetting(
                      context,
                      Icons.policy,
                      StringUtils.policy.tr,
                      () {
                        Get.defaultDialog(
                          backgroundColor: Theme.of(context).primaryColor,
                          title: StringUtils.policy.tr,
                          titlePadding: const EdgeInsets.all(20),
                          contentPadding:
                              const EdgeInsets.only(left: 20, right: 20),
                          titleStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 26,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .color),
                          content: SizedBox(
                            height: Get.height * 0.6,
                            width: Get.width,
                            child: SingleChildScrollView(
                              child: Text(
                                StringUtils.policy_text.tr,
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodySmall!
                                        .color),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    itemSetting(
                      context,
                      Icons.info,
                      StringUtils.about_us.tr,
                      () {
                        Get.defaultDialog(
                          backgroundColor: Theme.of(context).primaryColor,
                          title: StringUtils.about_us.tr,
                          titlePadding: const EdgeInsets.all(20),
                          titleStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 28,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .color),
                          content: SizedBox(
                            height: Get.height * 0.6,
                            width: Get.width,
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  SvgPicture.asset(IconUtils.icLogo),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    itemSetting(
                      context,
                      Icons.star_rate,
                      StringUtils.star_rate.tr,
                      () {
                        Get.defaultDialog(
                            title: StringUtils.notification.tr,
                            titleStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 28,
                                color: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .color),
                            titlePadding:
                                const EdgeInsets.only(top: 20, bottom: 20),
                            content: Text(StringUtils.developing_features.tr,style: TextStyle(color: Theme.of(context).textTheme.bodySmall!.color),));
                      },
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget itemSetting(
      BuildContext context, IconData icon, String text, VoidCallback callback) {
    return Material(
      color: Theme.of(context).primaryColor,
      child: InkWell(
        onTap: callback,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 30, right: 30, top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Icon(
                    icon,
                    color: Theme.of(context).iconTheme.color,
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Text(
                    text,
                    style: TextStyle(
                        color: Theme.of(context).textTheme.bodyMedium?.color,
                        fontSize: 16),
                  ),
                ],
              ),
              const Icon(
                Icons.arrow_forward_ios,
                size: 14,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
