import 'package:check_in/controller/setting_controller.dart';
import 'package:check_in/utils/color_utils.dart';
import 'package:check_in/utils/icons.dart';
import 'package:check_in/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DarkModePage extends StatelessWidget {
  const DarkModePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: GetBuilder<SettingController>(
          init: SettingController(),
          builder: (SettingController settingController) {
            return Column(
              children: [
                Material(
                  color: ColorUtils.primaryColor,
                  child: Row(
                    children: [
                      IconButton(
                        splashRadius: 20,
                        padding: const EdgeInsets.only(left: 10),
                        onPressed: () {
                          Get.back();
                        },
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text(
                          StringUtils.dark_mode.tr,
                          style: const TextStyle(
                              fontSize: 22, color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(25),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                IconUtils.icDark,
                                height: Get.width * 0.35,
                                fit: BoxFit.cover,
                              ),
                              SizedBox(
                                width: 100,
                                child: ListTile(
                                  title: Text(
                                    StringUtils.on.tr,
                                    style: TextStyle(
                                        color: Theme.of(context)
                                            .textTheme
                                            .bodyMedium!
                                            .color),
                                  ),
                                  leading: Radio<CheckDarkMode>(
                                    fillColor: MaterialStateColor.resolveWith(
                                        (states) => ColorUtils.primaryColor),
                                    focusColor: MaterialStateColor.resolveWith(
                                        (states) => ColorUtils.primaryColor),
                                    value: CheckDarkMode.on,
                                    groupValue: settingController.checkDarkMode,
                                    onChanged: (CheckDarkMode? value) {
                                      settingController.checkDarkMode = value!;
                                      settingController.changeThemeMode();
                                    },
                                  ),
                                  contentPadding: EdgeInsets.zero,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                IconUtils.icLight,
                                height: Get.width * 0.35,
                                fit: BoxFit.cover,
                              ),
                              SizedBox(
                                width: 100,
                                child: ListTile(
                                  title: Text(StringUtils.off.tr,
                                      style: TextStyle(
                                          color: Theme.of(context)
                                              .textTheme
                                              .bodyMedium!
                                              .color)),
                                  contentPadding: EdgeInsets.zero,
                                  leading: Radio<CheckDarkMode>(
                                    fillColor: MaterialStateColor.resolveWith(
                                        (states) => ColorUtils.primaryColor),
                                    focusColor: MaterialStateColor.resolveWith(
                                        (states) => ColorUtils.primaryColor),
                                    value: CheckDarkMode.off,
                                    groupValue: settingController.checkDarkMode,
                                    onChanged: (CheckDarkMode? value) {
                                      settingController.checkDarkMode = value!;
                                      settingController.changeThemeMode();
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const Divider(
                        color: Colors.grey,
                        thickness: 1,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          RichText(
                            text: TextSpan(
                              text: StringUtils.auto_change_interface.tr,
                              style: const TextStyle(
                                  height: 1.4,
                                  color: ColorUtils.primaryColor,
                                  fontSize: 16),
                              children: [
                                TextSpan(
                                  text: StringUtils.to_system_preferences.tr,
                                  style: const TextStyle(
                                      color: ColorUtils.shade60Color,
                                      fontSize: 14),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 70,
                            child: ListTile(
                              contentPadding: EdgeInsets.zero,
                              leading: Radio<CheckDarkMode>(
                                fillColor: MaterialStateColor.resolveWith(
                                    (states) => ColorUtils.primaryColor),
                                focusColor: MaterialStateColor.resolveWith(
                                    (states) => ColorUtils.primaryColor),
                                value: CheckDarkMode.none,
                                groupValue: settingController.checkDarkMode,
                                onChanged: (CheckDarkMode? value) {
                                  settingController.checkDarkMode = value!;
                                  settingController.changeThemeMode();
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
