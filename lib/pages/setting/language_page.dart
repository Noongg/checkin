import 'package:check_in/controller/setting_controller.dart';
import 'package:check_in/utils/color_utils.dart';
import 'package:check_in/utils/icons.dart';
import 'package:check_in/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class LanguagePage extends GetWidget<SettingController> {
  const LanguagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Material(
              color: ColorUtils.primaryColor,
              child: Row(
                children: [
                  IconButton(
                    splashRadius: 20,
                    padding: const EdgeInsets.only(left: 10),
                    onPressed: () {
                      Get.back();
                    },
                    icon: const Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Text(
                      StringUtils.language.tr,
                      style: const TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
            SvgPicture.asset(
              IconUtils.icLanguage,
              height: 160,
            ),
            const SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    StringUtils.choose_language.tr,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).textTheme.bodyMedium!.color),
                  ),
                  Text(
                    StringUtils.please_select_language.tr,
                    style: const TextStyle(
                        fontSize: 14,
                        height: 1.5,
                        color: ColorUtils.shade60Color),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
            Flexible(
              child: GetBuilder<SettingController>(
                  init: SettingController(),
                  builder: (SettingController settingController) {
                    return ListView.builder(
                      itemCount: settingController.listLanguage.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          contentPadding:
                              const EdgeInsets.only(right: 20, left: 25),
                          onTap: () {
                            settingController.indexLanguage = index;
                            settingController.changeLanguage(index);
                            settingController.update();
                          },
                          title: Row(
                            children: [
                              SvgPicture.asset(
                                settingController.listLanguage[index].icon,
                                height: 25,
                              ),
                              const SizedBox(
                                width: 30,
                              ),
                              Text(
                                settingController.listLanguage[index].name.tr,
                                style: TextStyle(
                                    color: Theme.of(context)
                                        .textTheme
                                        .bodyMedium!
                                        .color),
                              ),
                            ],
                          ),
                          trailing: settingController.indexLanguage == index
                              ? Icon(
                                  Icons.check,
                                  color: Theme.of(context).iconTheme.color,
                                )
                              : const Text(''),
                        );
                      },
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
