import 'package:check_in/controller/setting_controller.dart';
import 'package:check_in/widgets/buttons_widget.dart';
import 'package:check_in/widgets/text_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/color_utils.dart';
import '../../utils/strings.dart';

class ChangePasswordPage extends GetWidget<SettingController> {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: SizedBox(
            height: Get.height * 0.8,
            width: Get.width,
            child: Column(
              children: [
                Material(
                  color: ColorUtils.primaryColor,
                  child: Row(
                    children: [
                      IconButton(
                        splashRadius: 20,
                        padding: const EdgeInsets.only(left: 10),
                        onPressed: () {
                          Get.back();
                        },
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text(
                          StringUtils.change_password.tr,
                          style: const TextStyle(
                              fontSize: 22,
                              color: Colors.white),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(25),
                  child: GetBuilder<SettingController>(
                    init: SettingController(),
                    builder: (SettingController settingController) {
                      return Form(
                        key: controller.formKey,
                        child: Column(
                          children: [
                            _passwordOldField(),
                            const SizedBox(
                              height: 30,
                            ),
                            _passwordNewField(),
                            const SizedBox(
                              height: 20,
                            ),
                            _confirmPasswordField(),
                          ],
                        ),
                      );
                    }
                  ),
                ),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 32,right: 32),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: ButtonWidget(
                        onTap: () {
                          controller.changePassword();
                        },
                        title: StringUtils.change_password.tr,
                        height: 40,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _passwordOldField() {
    return GetBuilder<SettingController>(
      init: SettingController(),
      builder: (SettingController controller) {
        return TextFieldWidget(
          globalKey: controller.passwordOldKey,
          controller: controller.passwordOldEditingController,
          focusNode: controller.passwordOldFocusNode,
          obscureText: controller.hideOldPassWord,
          icon: IconButton(
              onPressed: () {
                controller.updateOldHidePassWord();
              },
              icon: Icon(controller.hideOldPassWord
                  ? Icons.visibility_off
                  : Icons.visibility)),
          title: StringUtils.password_old.tr,
          hintText: "* * * * * *",
          isRequired: true,
          validator: controller.validateOldPassword,
        );
      },
    );
  }

  Widget _passwordNewField() {
    return Column(
      children: [
        GetBuilder<SettingController>(
          init: SettingController(),
          builder: (SettingController controller) {
            return TextFieldWidget(
              globalKey: controller.passwordKey,
              focusNode: controller.passwordFocusNode,
              controller: controller.passwordEditingController,
              obscureText: controller.hidePassWord,
              title: StringUtils.password_new.tr,
              icon: IconButton(
                  onPressed: () {
                    controller.updateHidePassWord();
                  },
                  icon: Icon(controller.hidePassWord
                      ? Icons.visibility_off
                      : Icons.visibility)),
              hintText: "* * * * * *",
              isRequired: true,
              onChanged: controller.onChangedPass,
              validator: controller.validatePassword,
            );
          },
        ),
        GridView.builder(
          primary: false,
          shrinkWrap: true,
          padding: const EdgeInsets.only(top: 10),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: Get.width / 50,
          ),
          itemBuilder: (_, index) {
            return _itemVerify(
                isVerified: controller.listVerifyPass[index].isVerified,
                label: controller.listVerifyPass[index].label);
          },
          itemCount: controller.listVerifyPass.length,
        )
      ],
    );
  }

  Widget _itemVerify({required String label, required bool isVerified}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2.5),
            color: isVerified ? Colors.green : Colors.grey,
          ),
        ),
        const SizedBox(
          width: 4,
        ),
        Text(
          label,
          style: TextStyle(
            fontSize: 10,
            color: isVerified ? ColorUtils.successColor : ColorUtils.grey1Color,
          ),
        ),
      ],
    );
  }

  Widget _confirmPasswordField() {
    return GetBuilder<SettingController>(
      init: SettingController(),
      builder: (SettingController controller) {
        return TextFieldWidget(
          globalKey: controller.confirmPasswordKey,
          controller: controller.passwordConfirmEditingController,
          focusNode: controller.confirmPasswordFocusNode,
          obscureText: controller.hideConfirmPassWord,
          icon: IconButton(
              onPressed: () {
                controller.updateHidConfirmPassWord();
              },
              icon: Icon(controller.hideConfirmPassWord
                  ? Icons.visibility_off
                  : Icons.visibility)),
          title: StringUtils.confirm_password.tr,
          hintText: "* * * * * *",
          isRequired: true,
          validator: controller.validateConfirmPassword,
        );
      },
    );
  }
}
