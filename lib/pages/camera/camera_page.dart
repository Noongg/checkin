import 'package:camera/camera.dart';
import 'package:check_in/utils/strings.dart';
import 'package:check_in/widgets/loading_widget.dart';
import 'package:check_in/widgets/overlayShape.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../controller/camera_controller.dart';

class CameraPage extends StatelessWidget {
  CameraPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarIconBrightness: Brightness.dark,
      ),
      child: Scaffold(
        body: SafeArea(
          child: GetBuilder<ControllerCamera>(
            init: ControllerCamera(),
            builder: (ControllerCamera _camera) {
              if (_camera.initializing) {
                return Column(
                  children: [
                    Expanded(child: AspectRatio(
                      aspectRatio: _camera.cameraController!.value.aspectRatio,
                      child: CameraPreview(_camera.cameraController!),
                    )),
                    Container(
                      height: 90,
                      width: Get.width,
                      color: Colors.black,
                      child: Center(
                        child:FloatingActionButton(
                          child: const Icon(
                            Icons.camera,
                            color: Colors.black,
                          ),
                          backgroundColor: Colors.white,
                          onPressed: () async {
                            _camera.takePicture();
                          },
                        ),
                      )
                    )
                  ],
                );
              } else {
                return const Center(child: LoadingWidget());
              }
            },
          ),
        ),
      ),
    );
  }
}
