import 'package:check_in/helper/firebase_helper.dart';
import 'package:check_in/utils/color_utils.dart';
import 'package:check_in/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../../controller/home_controller.dart';
import '../../controller/time_controller.dart';
import '../../helper/storage_helper.dart';
import '../../routes/routes.dart';
import '../../utils/icons.dart';
import '../../widgets/loading_widget.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  final HomeController homeController = Get.find();

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(
          statusBarColor: ColorUtils.primaryColor,
          statusBarIconBrightness: Brightness.light),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: GetBuilder<HomeController>(
            init: HomeController(),
            builder: (HomeController controller) {
              return Stack(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomRight,
                      colors: [
                        ColorUtils.primaryColor,
                        Color(0xffE6639B),
                      ],
                    )),
                    height: Get.height,
                    width: Get.width,
                    padding:
                        const EdgeInsets.only(top: 10, bottom: 70, left: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Padding(padding: EdgeInsets.only(top: 20)),
                        homeController.avatarUser == null ||
                                homeController.isCheckInternet ==
                                    false
                            ? const CircleAvatar()
                            : ClipOval(
                                child: FadeInImage.assetNetwork(
                                    height: 60,
                                    width: 60,
                                    placeholder: 'assets/images/loading.gif',
                                    image: homeController.avatarUser!,
                                    fit: BoxFit.cover),
                              ),
                        const Padding(padding: EdgeInsets.only(top: 10)),
                        Text(
                          StringUtils.hi.tr,
                          style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontFamily: 'InterLight',
                              color: Colors.white,
                              fontSize: 12),
                        ),
                        Text(
                          homeController.nameUser!,
                          style: const TextStyle(
                              fontWeight: FontWeight.w700,
                              fontFamily: 'InterLight',
                              color: Colors.white,
                              height: 1.6,
                              fontSize: 12),
                        ),
                        const Padding(padding: EdgeInsets.only(top: 30)),
                        TextButton.icon(
                          style: ButtonStyle(
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.zero)),
                          onPressed: () {
                            homeController.closeDrawer();
                          },
                          icon: const Icon(
                            Icons.home,
                            color: Colors.white54,
                          ),
                          label: Text(
                            StringUtils.home_page.tr,
                            style: const TextStyle(
                                color: Colors.white54, fontSize: 13),
                          ),
                        ),
                        TextButton.icon(
                          style: ButtonStyle(
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.zero)),
                          onPressed: () {
                            Get.toNamed(Routes.PERSONAL_PAGE);
                            Future.delayed(const Duration(milliseconds: 500),
                                () {
                              homeController.closeDrawer();
                            });
                          },
                          icon: const Icon(
                            Icons.person,
                            color: Colors.white54,
                          ),
                          label: Text(
                            StringUtils.personal.tr,
                            style: const TextStyle(
                                color: Colors.white54, fontSize: 13),
                          ),
                        ),
                        TextButton.icon(
                          style: ButtonStyle(
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.zero)),
                          onPressed: () {
                            Get.toNamed(Routes.SETTING_PAGE);
                            Future.delayed(const Duration(milliseconds: 500),
                                () {
                              homeController.closeDrawer();
                            });
                          },
                          icon: const Icon(
                            Icons.settings,
                            color: Colors.white54,
                          ),
                          label: Text(
                            StringUtils.setting.tr,
                            style: const TextStyle(
                                color: Colors.white54, fontSize: 13),
                          ),
                        ),
                        Flexible(
                          child: Align(
                            alignment: Alignment.bottomLeft,
                            child: TextButton.icon(
                                style: ButtonStyle(
                                    padding: MaterialStateProperty.all(
                                        EdgeInsets.zero)),
                                onPressed: () {
                                  FirebaseHelper.auth.signOut();
                                  StorageHelper.box.erase();
                                  Get.delete<TimeController>();
                                  Get.offAllNamed(Routes.LOGIN_PAGE);
                                },
                                icon: const Icon(
                                  Icons.power_settings_new,
                                  color: Colors.white54,
                                ),
                                label: Text(
                                  StringUtils.log_out.tr,
                                  style: const TextStyle(color: Colors.white54),
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                  animateContainerWidget(
                      xOffset: homeController.xOffsetFourth,
                      yOffset: homeController.yOffsetFourth,
                      scaleFactor: homeController.scaleFactorFourth,
                      colors: Colors.white12),
                  animateContainerWidget(
                      xOffset: homeController.xOffsetThird,
                      yOffset: homeController.yOffsetThird,
                      scaleFactor: homeController.scaleFactorThird,
                      colors: Colors.white30),
                  animateContainerWidget(
                      xOffset: homeController.xOffsetSecond,
                      yOffset: homeController.yOffsetSecond,
                      scaleFactor: homeController.scaleFactorSecond,
                      colors: Colors.white54),
                  animateContainerWidget(
                    xOffset: homeController.xOffset,
                    yOffset: homeController.yOffset,
                    scaleFactor: homeController.scaleFactor,
                    colors: Colors.white,
                    widget: GestureDetector(
                      onTap: () {
                        homeController.closeDrawer();
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(
                                homeController.isDrawerOpen ? 25 : 0),
                            bottomLeft: Radius.circular(
                                homeController.isDrawerOpen ? 25 : 0),
                          ),
                        ),
                        child: LayoutBuilder(
                          builder: (context, view) {
                            return SingleChildScrollView(
                              child: Stack(
                                alignment: Alignment.topCenter,
                                children: [
                                  ConstrainedBox(
                                    constraints: BoxConstraints(
                                      minHeight: view.maxHeight,
                                    ),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          children: [
                                            Container(
                                              decoration: BoxDecoration(
                                                color: homeController
                                                        .isDrawerOpen
                                                    ? ColorUtils.blueColor
                                                    : ColorUtils.primaryColor,
                                                borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(
                                                      homeController
                                                              .isDrawerOpen
                                                          ? 25
                                                          : 0),
                                                ),
                                              ),
                                              height: 60,
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  homeController.isDrawerOpen
                                                      ? IconButton(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 30),
                                                          icon: const Icon(
                                                            Icons
                                                                .arrow_back_ios,
                                                            color: Colors.white,
                                                          ),
                                                          onPressed: () {
                                                            homeController
                                                                .closeDrawer();
                                                          },
                                                        )
                                                      : IconButton(
                                                          onPressed: () {
                                                            homeController
                                                                .openDrawer();
                                                          },
                                                          icon: const Icon(
                                                            Icons.menu,
                                                            color: Colors.white,
                                                          ),
                                                        ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      const Icon(
                                                          Icons.location_on,
                                                          color: Colors.white),
                                                      SizedBox(
                                                        width: Get.width * 0.4,
                                                        child: Text(
                                                            '${homeController.address}',
                                                            maxLines: 1,
                                                            style:
                                                                const TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                      )
                                                    ],
                                                  ),
                                                  Row(
                                                    children: [
                                                      homeController.avatarUser ==
                                                                  null ||
                                                              homeController
                                                                      .isCheckInternet ==
                                                                  false
                                                          ? const CircleAvatar()
                                                          : ClipOval(
                                                              child: FadeInImage.assetNetwork(
                                                                  height: 40,
                                                                  width: 40,
                                                                  placeholder:
                                                                      IconUtils
                                                                          .icLoading,
                                                                  image: homeController
                                                                      .avatarUser!,
                                                                  fit: BoxFit
                                                                      .cover),
                                                            ),
                                                      const Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                right: 10),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            controller.checkMap
                                                ? SizedBox(
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.35,
                                                    child: GoogleMap(
                                                      mapType: MapType.normal,
                                                      markers:
                                                          controller.marker,
                                                      initialCameraPosition:
                                                          CameraPosition(
                                                              target: LatLng(
                                                                  controller
                                                                      .latitude!,
                                                                  controller
                                                                      .longitude!),
                                                              zoom: 18),
                                                      onMapCreated:
                                                          (GoogleMapController
                                                              googleMapController) {
                                                        controller.mapController
                                                            .complete(
                                                                googleMapController);
                                                        controller
                                                            .setMapStyle();
                                                      },
                                                    ),
                                                  )
                                                : Container(
                                                    color: Colors.grey.shade200,
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.35,
                                                    child: const Center(
                                                      child: LoadingWidget(),
                                                    ),
                                                  ),
                                            const Padding(
                                              padding: EdgeInsets.only(top: 80),
                                            ),
                                            GetBuilder<TimeController>(
                                              id: 'checkTime',
                                              init: TimeController(),
                                              builder: (checkTime) => Column(
                                                children: [
                                                  Text(
                                                    checkTime.getHour,
                                                    style: const TextStyle(
                                                        fontSize: 24,
                                                        fontFamily:
                                                            'InterBold'),
                                                  ),
                                                  const Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 10)),
                                                  Text(
                                                    checkTime.getDayOfWeek,
                                                    style: const TextStyle(
                                                        fontSize: 14,
                                                        fontFamily:
                                                            'InterBold'),
                                                  ),
                                                  const Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 10)),
                                                ],
                                              ),
                                            ),
                                            StorageHelper.box.read(StorageHelper
                                                        .KEY_CHECK_IN) ==
                                                    null
                                                ? const SizedBox()
                                                : Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 30),
                                                    child: Text(
                                                      '${StringUtils.you_had_timekeeping_at.tr} ${StorageHelper.box.read(StorageHelper.KEY_SAVE_TIME)}',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color:
                                                              Theme.of(context)
                                                                  .textTheme
                                                                  .bodyMedium!
                                                                  .color,
                                                          fontFamily:
                                                              'InterLight'),
                                                    ),
                                                  ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 30),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              const Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 10)),
                                              customButton(
                                                  context: context,
                                                  callback: () async {
                                                    if (homeController
                                                        .isDrawerOpen) {
                                                      homeController
                                                          .closeDrawer();
                                                    }
                                                    Get.toNamed(Routes.ABSENT);
                                                  },
                                                  title:
                                                      StringUtils.on_leave.tr,
                                                  img: IconUtils.icMenu),
                                              customButton(
                                                  context: context,
                                                  callback: () {
                                                    if (homeController
                                                        .isDrawerOpen) {
                                                      homeController
                                                          .closeDrawer();
                                                    }
                                                    Get.toNamed(
                                                        Routes.TIME_SHEETS);
                                                  },
                                                  title: StringUtils
                                                      .time_sheets.tr,
                                                  img: IconUtils.icCalendar),
                                              const SizedBox(
                                                height: 10,
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    top: MediaQuery.of(context).size.height *
                                        0.32,
                                    child: GestureDetector(
                                      child: Container(
                                        height: 150,
                                        width: 150,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            color: const Color(0xff0F296D)),
                                        child: Center(
                                          child: StorageHelper.box.read(
                                                      StorageHelper
                                                          .KEY_CHECK_IN) ==
                                                  null
                                              ? Center(
                                                  child: Text(
                                                    StringUtils.check_in.tr,
                                                    style: const TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.white),
                                                  ),
                                                )
                                              : Center(
                                                  child: Text(
                                                    StringUtils.check_out.tr,
                                                    style: const TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.white),
                                                  ),
                                                ),
                                        ),
                                      ),
                                      onTap: () {
                                        controller.checkIn();
                                      },
                                    ),
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget animateContainerWidget(
      {double? xOffset,
      double? yOffset,
      double? scaleFactor,
      Color? colors,
      Widget? widget}) {
    return AnimatedContainer(
      transform: Matrix4.translationValues(xOffset!, yOffset!, 0)
        ..scale(scaleFactor!)
        ..rotateY(homeController.isDrawerOpen ? -0.6 : 0),
      duration: const Duration(milliseconds: 250),
      decoration: BoxDecoration(
          color: colors!,
          borderRadius:
              BorderRadius.circular(homeController.isDrawerOpen ? 25 : 0)),
      child: widget,
    );
  }

  Widget customButton(
      {required BuildContext context,
      required VoidCallback callback,
      required String title,
      required String img}) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        height: 70,
        width: 155,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).shadowColor,
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          children: [
            const Padding(padding: EdgeInsets.only(left: 10)),
            SvgPicture.asset(img),
            const Padding(padding: EdgeInsets.only(left: 10)),
            Flexible(
              child: Text(
                title,
                style: const TextStyle(
                    fontFamily: 'InterBold',
                    fontSize: 13,
                    color: ColorUtils.primaryColor),
              ),
            )
          ],
        ),
      ),
    );
  }
}
