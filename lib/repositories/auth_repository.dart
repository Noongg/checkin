import 'dart:convert';

import 'package:check_in/helper/api_helper.dart';
import 'package:check_in/utils/urls.dart';
import '../model/time_model.dart';

class AuthRepository {
  static Future<TimeModel?> getTimeZone() async {
    final result = await ApiBaseHelper()
        .getData(domain: UrlUtils.DOMAIN_TIME, url: UrlUtils.API_GET_TIME);
    if (result!.statusCode == 200) {
      return TimeModel.fromJson(result.body);
    } else {
      return null;
    }
  }

  static Future<bool> postSentMail(Map<String, dynamic> templateParams,String templateId) async {
    final result = await ApiBaseHelper().postData(
      domain: UrlUtils.DOMAIN_EMAIL,
      url: UrlUtils.API_SENT_EMAIL,
      headers: {'Content-type': 'application/json'},
      body: json.encode(
        {
          'service_id': 'service_25vocu7',
          'template_id': templateId,
          'user_id': '-vJQfwg0OUDCPlmc0',
          'template_params': templateParams,
          'accessToken':'6OZQ4iweEMEISUkKSNpdc'
        },
      ),
    );
    if (result!.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
