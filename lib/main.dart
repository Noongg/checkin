import 'package:camera/camera.dart';
import 'package:check_in/routes/pages.dart';
import 'package:check_in/routes/routes.dart';
import 'package:check_in/translations/translation.dart';
import 'package:check_in/utils/theme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'bindings/auth/login_binding.dart';
import 'controller/auth/auth_controller.dart';
import 'package:intl/date_symbol_data_local.dart';

List<CameraDescription> listCameras = [];

Future<void> main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  await Firebase.initializeApp();
  if (defaultTargetPlatform == TargetPlatform.android) {
    AndroidGoogleMapsFlutter.useAndroidViewSurface = true;
  }
  try {
    listCameras = await availableCameras();
  } on CameraException catch (e) {
    if (kDebugMode) {
      print(e.toString());
    }
  }
  await GetStorage.init();
  await GetStorage.init('theme');
  await GetStorage.init('language');
  Future.delayed(
    const Duration(seconds: 1),
    () {
      FlutterNativeSplash.remove();
    },
  );
  Get.put<AuthController>(AuthController());
  initializeDateFormatting().then((_) => runApp(MyApp()));
}

class MyApp extends StatelessWidget {
  final AuthController _controller = Get.find();

  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: _controller.isLogin ? Routes.HOMEPAGE : Routes.LOGIN_PAGE,
      initialBinding: LoginBinding(),
      themeMode: _controller.themeMode,
      darkTheme: MyThemes.darkTheme,
      theme: MyThemes.lightTheme,
      getPages: Pages.pages,
      locale:_controller.locale,
      translationsKeys: Translation.translations,
    );
  }
}
