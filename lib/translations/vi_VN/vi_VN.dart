// ignore_for_file: file_names

final Map<String, String> viVN = {
  "sign_in": "Đăng nhập",
  "sign_up": "Đăng ký",
  "username": "Tên đăng nhập",
  "email": "Email",
  "full_name": "Họ và tên",
  "password": "Mật khẩu",
  "remember_me": "Ghi nhớ tài khoản",
  "forgot_password": "Quên mật khẩu?",
  "don't_have_an_account": "Chưa có tài khoản?",
  "confirm_password": "Nhập lại mật khẩu",
  "subscribed_email": "Nhận tin tức và các thông tin dự án qua email",
  "agree_rules_1": "Tôi đã đọc và chấp nhận ",
  "agree_rules_2": "Thỏa thuận dịch vụ",
  "already_have_an_account": "Đã có tài khoản?",
  "not_enter_email_yet": "Bạn chưa nhập email",
  "not_enter_password_yet": "Bạn chưa nhập mật khẩu",
  "not_enter_full_name_yet": "Bạn chưa nhập họ tên",
  "not_enter_username_yet": "Bạn chưa nhập tên tài khoản",
  "password_not_match": "Mật khẩu không trùng khớp",
  "password_invalid": "Mật khẩu không đúng định dạng",
  "email_invalid": "Email không đúng định dạng",
  "username_invalid": "Tối thiểu 3 kí tự, gồm chữ thường và số",
  "full_name_invalid": "Họ tên phải có ít nhất 10 ký tự",
  "error": "Lỗi",
  "device_not_found": "Không tìm thấy thiết bị",
  "device_not_supported": "Thiết bị không hỗ trợ",
  "notification": "Thông báo",
  "disconnection_internet": "Ngắt kết nối internet",
  "lowercase_letters": "Có chữ thường",
  "digits": "Có chữ số",
  "capital_letters": "Có chữ hoa",
  "special_characters": "Có ký tự đặc biệt",
  "eight_characters": "Mật khẩu gồm ít nhất 8 ký tự",
  "monday": "Thứ 2",
  "tuesday": "Thứ 3",
  "wednesday": "Thứ 4",
  "thursday": "Thứ 5",
  "friday": "Thứ 6",
  "saturday": "Thứ 7",
  "sunday": "Chủ nhật",
  "day": "Ngày",
  "month": "Tháng",
  "year": "Năm",
  "no_user_found": "Không tìm thấy người dùng",
  "wrong_password": "Sai mật khẩu",
  "exists_email": "Email đã tồn tại",
  "add_your_photo": "Thêm ảnh của bạn",
  "continue_key": "Tiếp tục",
  "add_profile_picture":
      "Hãy thêm ảnh đại diện để chúng tôi\ndễ dàng nhận diện bạn",
  "choose_photo": "Chọn ảnh",
  "select_photo_from_gallery": "Chọn ảnh từ thư viện",
  "take_photo": "Chụp ảnh",
  "congratulations": "Chúc mừng!",
  "successfully_registered": "Bạn đã đăng ký thành công",
  "check_out": "Ra về",
  "check_in": "Vào làm",
  "timekeeping_success": "Chấm công thành công",
  "close": "Đóng",
  "you_had_timekeeping_at": "Bạn đã chấm công vào lúc",
  "on_leave": "Nghỉ phép",
  "morning": "Buổi sáng",
  "afternoon": "Buổi chiều",
  "not_entered_the_holiday": "Bạn chưa điền ngày nghỉ",
  "not_entered_the_holiday_from": "Bạn chưa điền ngày nghỉ từ",
  "not_entered_the_holiday_to": "Bạn chưa điền ngày nghỉ đến",
  "not_entered_the_reason": "Bạn chưa điền lý do",
  "email_sent_successfully": "Gửi mail thành công",
  "all_day": "Cả ngày",
  "be_late": "Đi muộn",
  "on_time": "Đúng giờ",
  "time": "Thời gian:",
  "one_day": "Một ngày",
  "days": "Nhiều ngày",
  "break_from": "Nghỉ từ:",
  "break_to": "Nghỉ đến:",
  "unsuccessful": "Không thành công",
  "reason": "Lý do:",
  "day_off": "Ngày nghỉ:",
  "the_break": "Buổi nghỉ:",
  "car_failure": "Xin nghỉ phép vì hỏng xe",
  "time_sheets": "Lịch chấm công",
  "workday": "Ngày công",
  "off_work": "Nghỉ làm",
  "move_camera": "Di chuyển camera vào vị trí\nkhuôn mặt",
  "hi": "Xin chào!",
  "home_page": "Trang chủ",
  "personal": " Trang Cá nhân",
  "feedback": "Phản hồi",
  "total_working": "Tổng giờ làm",
  "log_out": "Đăng xuất",
  "setting": "Cài đặt",
  "send": "Gửi Mail",
  "open_mail":
      "Đừng lo lắng! Hãy nhập email đã đăng ký của bạn để nhận hướng dẫn đặt lại mật khẩu.",
  "email_has_sent": "Email đã được gửi!",
  "check_email": "Xin vui lòng kiểm tra email của bạn",
  "have_sent_recover_pass":
      "Chúng tôi đã gửi hướng dẫn khôi phục mật khẩu\ntới email của bạn.",
  "back_to_login": "Quay lại đăng nhập",
  "not_rule": "Bạn chưa chấp nhận thoả thuận dịch vụ",
  "dark_mode": "Chế độ tối",
  "status": "Trạng thái",
  "language": "Ngôn ngữ",
  "policy": "Chính sách và quyền riêng tư",
  "about_us": "Về chúng tôi",
  "star_rate": "Đánh giá",
  "have_worked": "Đã làm",
  "today": "Hôm nay",
  "this_month": "Tháng này",
  "of": "của",
  "on": "Bật",
  "off": "Tắt",
  "worked_enough_hours": "Đã làm đủ giờ",
  "not_worked_enough_hours": "Chưa làm đủ giờ",
  "auto_change_interface": "Tự động thay đổi giao diện\n",
  "to_system_preferences": "Theo tuỳ chọn hệ thống",
  "choose_language": "Chọn ngôn ngữ ưa thích của bạn",
  "please_select_language": "Xin chọn ngôn ngữ của bạn",
  "english": "Tiếng Anh",
  "hours": "Giờ",
  "time_chart_of_month": "Biểu đồ thời gian làm việc các tháng",
  "vietnamese": "Tiếng Việt",
  "change_password": "Đổi mật khẩu",
  "password_old": "Mật khẩu hiện tại",
  "password_match": "Mật khẩu mới trùng mật khẩu cũ",
  "password_new": "Mật khẩu mới",
  "developing_features": "Tính năng đang phát triển",
  "logging_strange_device": "Bạn đang đăng nhập trên thiết bị lạ",
  "policy_text": '''Cập nhật lần cuối: ngày 17 tháng 5 năm 2022

Chính sách Bảo mật này mô tả các chính sách và thủ tục của Chúng tôi về việc thu thập, sử dụng và tiết lộ thông tin của Bạn khi Bạn sử dụng Dịch vụ và cho Bạn biết về các quyền riêng tư của Bạn và cách luật pháp bảo vệ Bạn.

Chúng tôi sử dụng dữ liệu Cá nhân của Bạn để cung cấp và cải thiện Dịch vụ. Bằng việc sử dụng Dịch vụ, Bạn đồng ý với việc thu thập và sử dụng thông tin theo Chính sách Bảo mật này. Chính sách Bảo mật này đã được tạo với sự trợ giúp của Trình tạo Chính sách Bảo mật .

Giải thích và Định nghĩa
Diễn dịch
Những từ có chữ cái đầu tiên được viết hoa có nghĩa được xác định trong các điều kiện sau. Các định nghĩa sau đây sẽ có cùng ý nghĩa bất kể chúng xuất hiện ở số ít hay số nhiều.

Định nghĩa
Đối với các mục đích của Chính sách Bảo mật này:

Tài khoản có nghĩa là một tài khoản duy nhất được tạo để Bạn truy cập Dịch vụ của chúng tôi hoặc các phần của Dịch vụ của chúng tôi.

Đơn vị liên kết có nghĩa là một tổ chức kiểm soát, chịu sự kiểm soát của hoặc dưới sự kiểm soát chung của một bên, trong đó "quyền kiểm soát" có nghĩa là quyền sở hữu từ 50% trở lên cổ phần, lợi tức cổ phần hoặc chứng khoán khác được quyền bỏ phiếu bầu giám đốc hoặc cơ quan quản lý khác .

Ứng dụng có nghĩa là chương trình phần mềm do Công ty cung cấp được Bạn tải xuống trên bất kỳ thiết bị điện tử nào, có tên là checkin

Công ty (được gọi là "Công ty", "Chúng tôi", "Chúng tôi" hoặc "Của chúng tôi" trong Thỏa thuận này) đề cập đến đăng ký.

Quốc gia đề cập đến: Việt Nam

Thiết bị có nghĩa là bất kỳ thiết bị nào có thể truy cập Dịch vụ như máy tính, điện thoại di động hoặc máy tính bảng kỹ thuật số.

Dữ liệu Cá nhân là bất kỳ thông tin nào liên quan đến một cá nhân được xác định hoặc nhận dạng được.

Dịch vụ đề cập đến Ứng dụng.

Nhà cung cấp dịch vụ có nghĩa là bất kỳ thể nhân hoặc pháp nhân nào xử lý dữ liệu thay mặt cho Công ty. Nó đề cập đến các công ty bên thứ ba hoặc cá nhân được Công ty thuê để hỗ trợ Dịch vụ, cung cấp Dịch vụ thay mặt Công ty, thực hiện các dịch vụ liên quan đến Dịch vụ hoặc hỗ trợ Công ty phân tích cách Dịch vụ được sử dụng.

Dữ liệu sử dụng đề cập đến dữ liệu được thu thập tự động, được tạo ra bởi việc sử dụng Dịch vụ hoặc từ chính cơ sở hạ tầng Dịch vụ (ví dụ: thời lượng của một lượt truy cập trang).

Bạn có nghĩa là cá nhân truy cập hoặc sử dụng Dịch vụ, hoặc công ty hoặc pháp nhân khác thay mặt cho cá nhân đó đang truy cập hoặc sử dụng Dịch vụ, nếu có.

Thu thập và sử dụng dữ liệu cá nhân của bạn
Các loại dữ liệu được thu thập
Dữ liệu cá nhân
Trong khi sử dụng Dịch vụ của Chúng tôi, Chúng tôi có thể yêu cầu Bạn cung cấp cho Chúng tôi một số thông tin nhận dạng cá nhân nhất định có thể được sử dụng để liên hệ hoặc nhận dạng Bạn. Thông tin nhận dạng cá nhân có thể bao gồm, nhưng không giới hạn ở:

Địa chỉ email

Tên và họ

Dữ liệu sử dụng

Dữ liệu sử dụng
Dữ liệu sử dụng được thu thập tự động khi sử dụng Dịch vụ.

Dữ liệu sử dụng có thể bao gồm thông tin như địa chỉ Giao thức Internet trên thiết bị của Bạn (ví dụ: địa chỉ IP), loại trình duyệt, phiên bản trình duyệt, các trang Dịch vụ của chúng tôi mà Bạn truy cập, ngày và giờ truy cập của Bạn, thời gian dành cho các trang đó, thiết bị duy nhất số nhận dạng và dữ liệu chẩn đoán khác.

Khi Bạn truy cập Dịch vụ bằng hoặc thông qua thiết bị di động, Chúng tôi có thể tự động thu thập một số thông tin nhất định, bao gồm nhưng không giới hạn ở loại thiết bị di động Bạn sử dụng, ID duy nhất trên thiết bị di động của Bạn, địa chỉ IP của thiết bị di động của Bạn, Điện thoại di động của Bạn hệ điều hành, loại trình duyệt Internet di động Bạn sử dụng, số nhận dạng thiết bị duy nhất và dữ liệu chẩn đoán khác.

Chúng tôi cũng có thể thu thập thông tin mà trình duyệt của Bạn gửi bất cứ khi nào Bạn truy cập Dịch vụ của chúng tôi hoặc khi Bạn truy cập Dịch vụ bằng hoặc thông qua thiết bị di động.

Thông tin được thu thập khi sử dụng ứng dụng
Trong khi sử dụng Ứng dụng của Chúng tôi, để cung cấp các tính năng của Ứng dụng của Chúng tôi, Chúng tôi có thể thu thập, với sự cho phép trước của Bạn:

Thông tin liên quan đến vị trí của bạn

Ảnh và thông tin khác từ máy ảnh và thư viện ảnh trên Thiết bị của bạn

Chúng tôi sử dụng thông tin này để cung cấp các tính năng của Dịch vụ của chúng tôi, để cải thiện và tùy chỉnh Dịch vụ của chúng tôi. Thông tin có thể được tải lên máy chủ của Công ty và / hoặc máy chủ của Nhà cung cấp dịch vụ hoặc nó có thể được lưu trữ đơn giản trên thiết bị của Bạn.

Bạn có thể bật hoặc tắt quyền truy cập vào thông tin này bất kỳ lúc nào thông qua cài đặt Thiết bị của bạn.

Sử dụng dữ liệu cá nhân của bạn
Công ty có thể sử dụng Dữ liệu Cá nhân cho các mục đích sau:

Để cung cấp và duy trì Dịch vụ của chúng tôi , bao gồm cả việc giám sát việc sử dụng Dịch vụ của chúng tôi.

Để quản lý Tài khoản của bạn: để quản lý đăng ký của Bạn với tư cách là người dùng Dịch vụ. Dữ liệu Cá nhân Bạn cung cấp có thể cung cấp cho Bạn quyền truy cập vào các chức năng khác nhau của Dịch vụ khả dụng cho Bạn với tư cách là người dùng đã đăng ký.

Đối với việc thực hiện hợp đồng: sự phát triển, tuân thủ và cam kết của hợp đồng mua bán các sản phẩm, mặt hàng hoặc dịch vụ mà Bạn đã mua hoặc bất kỳ hợp đồng nào khác với Chúng tôi thông qua Dịch vụ.

Để liên hệ với Bạn: Để liên hệ với Bạn bằng email, cuộc gọi điện thoại, SMS hoặc các hình thức liên lạc điện tử tương đương khác, chẳng hạn như thông báo đẩy của ứng dụng di động về các bản cập nhật hoặc thông tin liên lạc liên quan đến các chức năng, sản phẩm hoặc dịch vụ đã ký hợp đồng, bao gồm các bản cập nhật bảo mật, khi cần thiết hoặc hợp lý để thực hiện chúng.

Để cung cấp cho Bạn tin tức, ưu đãi đặc biệt và thông tin chung về hàng hóa, dịch vụ và sự kiện khác mà chúng tôi cung cấp tương tự như những sản phẩm mà bạn đã mua hoặc yêu cầu trừ khi Bạn đã chọn không nhận thông tin đó.

Để quản lý các yêu cầu của Bạn: Để tham dự và quản lý các yêu cầu của Bạn với Chúng tôi.

Đối với chuyển giao doanh nghiệp: Chúng tôi có thể sử dụng thông tin của Bạn để đánh giá hoặc tiến hành sáp nhập, giải thể, tái cơ cấu, tổ chức lại, giải thể hoặc bán hoặc chuyển nhượng một số hoặc tất cả tài sản của Chúng tôi, cho dù là hoạt động liên tục hay là một phần của phá sản, thanh lý, hoặc thủ tục tương tự, trong đó Dữ liệu Cá nhân do Chúng tôi nắm giữ về người dùng Dịch vụ của chúng tôi nằm trong số các tài sản được chuyển giao.

Cho các mục đích khác : Chúng tôi có thể sử dụng thông tin của Bạn cho các mục đích khác, chẳng hạn như phân tích dữ liệu, xác định xu hướng sử dụng, xác định hiệu quả của các chiến dịch khuyến mại và để đánh giá và cải thiện Dịch vụ của chúng tôi, sản phẩm, dịch vụ, tiếp thị và trải nghiệm của bạn.

Chúng tôi có thể chia sẻ thông tin cá nhân của Bạn trong các trường hợp sau:

Với Nhà cung cấp dịch vụ: Chúng tôi có thể chia sẻ thông tin cá nhân của Bạn với Nhà cung cấp dịch vụ để theo dõi và phân tích việc sử dụng Dịch vụ của chúng tôi, để liên hệ với Bạn.
Đối với chuyển giao kinh doanh: Chúng tôi có thể chia sẻ hoặc chuyển thông tin cá nhân của Bạn liên quan đến, hoặc trong quá trình đàm phán, bất kỳ việc sáp nhập, bán tài sản của Công ty, tài trợ hoặc mua lại toàn bộ hoặc một phần hoạt động kinh doanh của Chúng tôi cho một công ty khác.
Với các Chi nhánh: Chúng tôi có thể chia sẻ thông tin của Bạn với các chi nhánh của Chúng tôi, trong trường hợp đó, chúng tôi sẽ yêu cầu các chi nhánh đó tuân theo Chính sách Bảo mật này. Các chi nhánh bao gồm công ty mẹ của Chúng tôi và bất kỳ công ty con nào khác, các đối tác liên doanh hoặc các công ty khác mà Chúng tôi kiểm soát hoặc nằm dưới sự kiểm soát chung của Chúng tôi.
Với các đối tác kinh doanh: Chúng tôi có thể chia sẻ thông tin của Bạn với các đối tác kinh doanh của Chúng tôi để cung cấp cho Bạn các sản phẩm, dịch vụ hoặc chương trình khuyến mãi nhất định.
Với những người dùng khác: khi Bạn chia sẻ thông tin cá nhân hoặc tương tác ở các khu vực công cộng với những người dùng khác, những thông tin đó có thể được tất cả người dùng xem và có thể được phân phối công khai ra bên ngoài.
Với sự đồng ý của Bạn : Chúng tôi có thể tiết lộ thông tin cá nhân của Bạn cho bất kỳ mục đích nào khác với sự đồng ý của Bạn.
Lưu giữ dữ liệu cá nhân của bạn
Công ty sẽ chỉ lưu giữ Dữ liệu Cá nhân của Bạn chừng nào cần thiết cho các mục đích được nêu trong Chính sách Bảo mật này. Chúng tôi sẽ lưu giữ và sử dụng Dữ liệu Cá nhân của Bạn trong phạm vi cần thiết để tuân thủ các nghĩa vụ pháp lý của chúng tôi (ví dụ: nếu chúng tôi được yêu cầu giữ lại dữ liệu của bạn để tuân thủ luật hiện hành), giải quyết tranh chấp và thực thi các thỏa thuận và chính sách pháp lý của chúng tôi.

Công ty cũng sẽ giữ lại Dữ liệu sử dụng cho các mục đích phân tích nội bộ. Dữ liệu sử dụng thường được lưu giữ trong một khoảng thời gian ngắn hơn, ngoại trừ khi dữ liệu này được sử dụng để tăng cường bảo mật hoặc để cải thiện chức năng của Dịch vụ của chúng tôi hoặc Chúng tôi có nghĩa vụ pháp lý phải lưu giữ dữ liệu này trong khoảng thời gian dài hơn.

Chuyển dữ liệu cá nhân của bạn
Thông tin của bạn, bao gồm cả Dữ liệu Cá nhân, được xử lý tại các văn phòng điều hành của Công ty và ở bất kỳ nơi nào khác có trụ sở của các bên liên quan đến việc xử lý. Điều đó có nghĩa là thông tin này có thể được chuyển đến - và duy trì trên - các máy tính đặt bên ngoài tiểu bang, tỉnh, quốc gia của Bạn hoặc khu vực tài phán khác của chính phủ, nơi luật bảo vệ dữ liệu có thể khác với luật trong khu vực tài phán của Bạn.

Sự đồng ý của Bạn đối với Chính sách Bảo mật này, sau đó là việc Bạn gửi thông tin đó thể hiện sự đồng ý của Bạn đối với việc chuyển giao đó.

Công ty sẽ thực hiện tất cả các bước cần thiết một cách hợp lý để đảm bảo rằng dữ liệu của Bạn được xử lý an toàn và phù hợp với Chính sách quyền riêng tư này và sẽ không có việc chuyển Dữ liệu Cá nhân của Bạn cho một tổ chức hoặc một quốc gia trừ khi có các biện pháp kiểm soát thích hợp bao gồm cả bảo mật của Dữ liệu của bạn và thông tin cá nhân khác.

Tiết lộ dữ liệu cá nhân của bạn
Giao dịch kinh doanh
Nếu Công ty tham gia vào việc sáp nhập, mua lại hoặc bán tài sản, Dữ liệu Cá nhân của Bạn có thể được chuyển giao. Chúng tôi sẽ cung cấp thông báo trước khi Dữ liệu Cá nhân của Bạn được chuyển giao và trở thành đối tượng của Chính sách Bảo mật khác.

Thực thi pháp luật
Trong một số trường hợp nhất định, Công ty có thể được yêu cầu tiết lộ Dữ liệu Cá nhân của Bạn nếu luật pháp yêu cầu làm như vậy hoặc theo yêu cầu hợp lệ của cơ quan công quyền (ví dụ: tòa án hoặc cơ quan chính phủ).

Các yêu cầu pháp lý khác
Công ty có thể tiết lộ Dữ liệu Cá nhân của Bạn với thiện chí tin rằng hành động đó là cần thiết để:

Tuân thủ nghĩa vụ pháp lý
Bảo vệ quyền lợi của Công ty
Ngăn chặn hoặc điều tra hành vi sai trái có thể xảy ra liên quan đến Dịch vụ
Bảo vệ sự an toàn cá nhân của Người dùng Dịch vụ hoặc công chúng
Bảo vệ khỏi trách nhiệm pháp lý
Bảo mật dữ liệu cá nhân của bạn
Sự bảo mật của Dữ liệu Cá nhân của Bạn là quan trọng đối với Chúng tôi, nhưng hãy nhớ rằng không có phương thức truyền tải nào qua Internet hoặc phương pháp lưu trữ điện tử là an toàn 100%. Trong khi Chúng tôi cố gắng sử dụng các phương tiện được chấp nhận về mặt thương mại để bảo vệ Dữ liệu Cá nhân của Bạn, Chúng tôi không thể đảm bảo tính bảo mật tuyệt đối của Dữ liệu đó.

Thông tin chi tiết về việc xử lý dữ liệu cá nhân của bạn
Các Nhà cung cấp Dịch vụ mà Chúng tôi sử dụng có thể có quyền truy cập vào Dữ liệu Cá nhân của Bạn. Các nhà cung cấp bên thứ ba này thu thập, lưu trữ, sử dụng, xử lý và chuyển thông tin về hoạt động của Bạn trên Dịch vụ của Chúng tôi theo Chính sách Bảo mật của họ.

Sử dụng, Hiệu suất và Các điều khoản khác
Chúng tôi có thể sử dụng Nhà cung cấp dịch vụ bên thứ ba để cải thiện Dịch vụ của mình tốt hơn.

Google Địa điểm

Google Địa điểm là một dịch vụ trả về thông tin về các địa điểm bằng cách sử dụng các yêu cầu HTTP. Nó được vận hành bởi Google

Dịch vụ Google Địa điểm có thể thu thập thông tin từ Bạn và từ Thiết bị của Bạn cho các mục đích bảo mật.

Thông tin do Google Địa điểm thu thập được lưu giữ theo Chính sách bảo mật của Google: https://www.google.com/intl/vi/policies/privacy/

Quyền riêng tư của trẻ em
Dịch vụ của chúng tôi không giải quyết bất kỳ ai dưới 13 tuổi. Chúng tôi không cố ý thu thập thông tin nhận dạng cá nhân từ bất kỳ ai dưới 13 tuổi. Nếu Bạn là cha mẹ hoặc người giám hộ và Bạn biết rằng con Bạn đã cung cấp Dữ liệu Cá nhân cho Chúng tôi, vui lòng liên hệ chúng tôi. Nếu Chúng tôi biết rằng Chúng tôi đã thu thập Dữ liệu Cá nhân từ bất kỳ ai dưới 13 tuổi mà không có sự xác minh của sự đồng ý của cha mẹ, Chúng tôi sẽ thực hiện các bước để xóa thông tin đó khỏi máy chủ của Chúng tôi.

Nếu Chúng tôi cần dựa trên sự đồng ý làm cơ sở pháp lý để xử lý thông tin của Bạn và Quốc gia của Bạn yêu cầu sự đồng ý từ cha mẹ, Chúng tôi có thể yêu cầu sự đồng ý của cha mẹ Bạn trước khi Chúng tôi thu thập và sử dụng thông tin đó.

Liên kết đến các trang web khác
Dịch vụ của Chúng tôi có thể chứa các liên kết đến các trang web khác không do Chúng tôi điều hành. Nếu Bạn nhấp vào liên kết của bên thứ ba, Bạn sẽ được dẫn đến trang web của bên thứ ba đó. Chúng tôi đặc biệt khuyên Bạn nên xem lại Chính sách Bảo mật của mọi trang web Bạn truy cập.

Chúng tôi không kiểm soát và không chịu trách nhiệm về nội dung, chính sách bảo mật hoặc thông lệ của bất kỳ trang web hoặc dịch vụ nào của bên thứ ba.

Các thay đổi đối với Chính sách quyền riêng tư này
Chúng tôi có thể cập nhật Chính sách quyền riêng tư của mình theo thời gian. Chúng tôi sẽ thông báo cho Bạn về bất kỳ thay đổi nào bằng cách đăng Chính sách Bảo mật mới trên trang này.

Chúng tôi sẽ cho Bạn biết qua email và / hoặc một thông báo nổi bật trên Dịch vụ của Chúng tôi, trước khi thay đổi có hiệu lực và cập nhật ngày "Cập nhật lần cuối" ở đầu Chính sách Bảo mật này.

Bạn nên xem lại Chính sách Bảo mật này định kỳ để biết bất kỳ thay đổi nào. Các thay đổi đối với Chính sách Bảo mật này có hiệu lực khi chúng được đăng trên trang này.

Liên hệ chúng tôi
Nếu bạn có bất kỳ câu hỏi nào về Chính sách Bảo mật này, Bạn có thể liên hệ với chúng tôi:

Qua email: long.nguyen@lifesup.com.vn''',
};
