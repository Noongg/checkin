import 'package:check_in/controller/auth/forgot_password_controller.dart';
import 'package:get/get.dart';

class ForgotPasswordBindings extends Bindings {

  @override
  void dependencies() {
    Get.put<ForgotPasswordController>(ForgotPasswordController());
  }
}