import 'package:check_in/controller/time_sheets_controller.dart';
import 'package:get/get.dart';

class TimeSheetsBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<TimeSheetsController>(TimeSheetsController());
  }
}