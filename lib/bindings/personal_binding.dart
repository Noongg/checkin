import 'package:check_in/controller/personal_controller.dart';
import 'package:get/get.dart';

class PersonalBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<PersonalController>(PersonalController());
  }

}