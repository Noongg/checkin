import 'package:check_in/controller/absent_controller.dart';
import 'package:get/get.dart';

class AbsentBindings extends Bindings {

  @override
  void dependencies() {
    Get.put<AbsentController>(AbsentController());
  }
}