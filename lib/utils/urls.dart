// ignore_for_file: non_constant_identifier_names

class UrlUtils {
  static String PROTOCOL = "https://";
  static String DOMAIN_TIME = "worldtimeapi.org";
  static String API_GET_TIME = "/api/timezone/Asia/Ho_Chi_Minh";
  static String DOMAIN_EMAIL = "api.emailjs.com";
  static String API_SENT_EMAIL = "/api/v1.0/email/send";
  static String urlConnect(String domain, String pathUrl) {
    return "$PROTOCOL$domain$pathUrl";
  }
}
