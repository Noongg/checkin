class IconUtils {
  static String icSuccess = 'assets/images/complete.svg';
  static String icComplete = 'assets/images/Success.svg';
  static String icMenu = 'assets/images/Group 33292.svg';
  static String icCalendar = 'assets/images/Group 33294.svg';
  static String icLogo = 'assets/images/logo.svg';
  static String icFrames = 'assets/images/Rectangle 11.svg';
  static String bgSplash = "assets/images/bg_splash.png";
  static String icLoading = 'assets/images/loading.gif';
  static String icForgot = 'assets/images/forgot.png';
  static String icOpenEmail = 'assets/images/email_open.svg';
  static String icLanguage = 'assets/images/language.svg';
  static String icGreatBritain = 'assets/images/great_britain.svg';
  static String icVietnam = 'assets/images/vietnam.svg';
  static String icDark = 'assets/images/dark.png';
  static String icLight = 'assets/images/light.png';
}
