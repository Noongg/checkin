import 'package:flutter/material.dart';

import 'color_utils.dart';

class MyThemes {
  static final darkTheme = ThemeData(
    brightness: Brightness.dark,
    shadowColor: Colors.transparent,
    primaryColor: Colors.black,
    backgroundColor: Colors.grey.shade600,
    iconTheme: const IconThemeData(color: ColorUtils.shade60Color),
    textTheme:  const TextTheme(
      bodySmall:  TextStyle(color: ColorUtils.shade60Color),
      bodyLarge:  TextStyle(color: Colors.white),
      bodyMedium:  TextStyle(color: ColorUtils.shade60Color),
    ),
  );
  static final lightTheme = ThemeData(
    brightness: Brightness.light,
    shadowColor: Colors.grey.withOpacity(0.5),
    primaryColor: Colors.white,
    backgroundColor: Colors.white,
    iconTheme: const IconThemeData(color: ColorUtils.primaryColor),
    textTheme:  const TextTheme(
      bodySmall:TextStyle(color: Colors.black),
      bodyLarge:TextStyle(color: Colors.black),
      bodyMedium:TextStyle(color: ColorUtils.primaryColor),
    ),
  );
}
