import 'package:check_in/helper/storage_helper.dart';
import 'package:check_in/utils/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';

import '../controller/time_controller.dart';
import '../routes/routes.dart';

class FirebaseHelper {
  static FirebaseAuth auth = FirebaseAuth.instance;
  static FirebaseFirestore fireStore = FirebaseFirestore.instance;

  static Future<void> loginFirebase(
      {required String email, required String password}) async {
    try {
      UserCredential userCredential = await auth.signInWithEmailAndPassword(
          email: email, password: password);
      userCredential.user;
      Get.offAllNamed(Routes.HOMEPAGE);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        Get.back();
        Get.snackbar(StringUtils.error.tr, StringUtils.no_user_found.tr);
      } else if (e.code == 'wrong-password') {
        Get.back();
        Get.snackbar(StringUtils.error.tr, StringUtils.wrong_password.tr);
      }
    }
  }

  static Future<void> registerFirebase(
      {required String name,
      required String email,
      required String password}) async {
    User? user;
    try {
      UserCredential userCredential = await auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      user = userCredential.user;
      await user!.sendEmailVerification();
      await user.updateDisplayName(name);
      await user.reload();
      await fireStore
          .collection("user")
          .doc(auth.currentUser!.uid)
          .set({}).then((_) {});
      Get.back();
      Get.offAllNamed(Routes.SUCCESS_PAGE);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'email-already-in-use') {
        Get.back();
        Get.snackbar(StringUtils.error.tr, StringUtils.exists_email.tr);
      }
    }
  }

  static Future<QueryDocumentSnapshot?> getLastTimeKeeping() async {
    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("user")
          .doc(auth.currentUser!.uid)
          .collection('timeKeeping')
          .get();
      try {
        QueryDocumentSnapshot allData = querySnapshot.docs.last;
        return allData;
      } catch (e) {
        return null;
      }
    } on FirebaseAuthException catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
      return null;
    }
  }
  static Future<QuerySnapshot?> getAllTimeKeeping() async {
    try {
      QuerySnapshot querySnapshot = await fireStore
          .collection("user")
          .doc(auth.currentUser!.uid)
          .collection('timeKeeping')
          .get();
      return querySnapshot;
    } on FirebaseAuthException catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
      return null;
    }
  }

  static Future<void> changePassword(String passwordOld, String passwordNew) async {
    try {
      try {
        UserCredential userCredential = await auth.signInWithEmailAndPassword(
            email: auth.currentUser!.email!, password: passwordOld);
        userCredential.user;
        auth.currentUser!.updatePassword(passwordNew);
        FirebaseHelper.auth.signOut();
        StorageHelper.box.erase();
        Get.delete<TimeController>();
        Get.offAllNamed(Routes.LOGIN_PAGE);
      } on FirebaseAuthException catch (e) {
        if (e.code == 'wrong-password') {
          Get.back();
          Get.snackbar(StringUtils.error.tr, StringUtils.wrong_password.tr);
        }
      }
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }
    }
  }
}
