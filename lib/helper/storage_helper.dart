// ignore_for_file: constant_identifier_names
import 'package:get_storage/get_storage.dart';

class StorageHelper {
  static GetStorage box = GetStorage();
  static GetStorage boxLanguage = GetStorage('language');
  static GetStorage boxTheme = GetStorage('theme');
  static const String KEY_CHECK_IN = "check_in";
  static const String KEY_TIME_KEEPING = "check_time_keeping";
  static const String KEY_SAVE_TIME = "save_time";
  static const String KEY_SAVE_LOCATION = "save_location";
  static const String KEY_SAVE_CHECK_IN = "save_check_in";
  static const String KEY_THEME_MODE = "theme_mode";
  static const String KEY_LANGUAGE = "language";
}
