import 'package:check_in/utils/urls.dart';
import 'package:get/get.dart';

class ApiBaseHelper extends GetConnect {
  static Map<String, String> getDefaultHeader() {
    return {
      'Content-type': 'application/json;charset=UTF-8',
      'Accept': 'application/json',
    };
  }

  Future<Response?> getData({
    Map<String, String>? headers,
    Map<String, dynamic>? params,
    required String domain,
    required String url,
  }) async {
    try {
      Response response = await get(
        UrlUtils.urlConnect(domain, url),
        headers: headers ?? getDefaultHeader(),
        query: params ?? {},
      );
      switch (response.statusCode) {
        case 401:
          return null;
        case 403:
          return null;
        default:
          return response;
      }
    } catch (exception) {
      return null;
    }
  }

  Future<Response?> postData({
    Map<String, String>? headers,
    Map<String, dynamic>? params,
    required dynamic body,
    required String domain,
    required String url,
  }) async {
    try {
      Response response = await post(
        UrlUtils.urlConnect(domain, url),
        body ?? {},
        headers: headers ?? getDefaultHeader(),
        query: params ?? {},
      );
      switch (response.statusCode) {
        case 401:
          return null;
        case 403:
          return null;
        default:
          return response;
      }
    } catch (exception) {
      return null;
    }
  }
}
