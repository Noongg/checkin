import 'package:check_in/bindings/absent_binding.dart';
import 'package:check_in/bindings/auth/forgot_password_binding.dart';
import 'package:check_in/bindings/home_binding.dart';
import 'package:check_in/bindings/auth/register_binding.dart';
import 'package:check_in/bindings/personal_binding.dart';
import 'package:check_in/bindings/setting_binding.dart';
import 'package:check_in/bindings/time_sheets_binding.dart';
import 'package:check_in/pages/absent/absent_page.dart';
import 'package:check_in/pages/auth/forgot_password_page.dart';
import 'package:check_in/pages/auth/email_reset_password_page.dart';
import 'package:check_in/pages/auth/register_page.dart';
import 'package:check_in/pages/auth/success_page.dart';
import 'package:check_in/pages/personal/personal_page.dart';
import 'package:check_in/pages/setting/change_password_page.dart';
import 'package:check_in/pages/setting/dark_mode_page.dart';
import 'package:check_in/pages/setting/language_page.dart';
import 'package:check_in/pages/setting/setting_page.dart';
import 'package:check_in/pages/time_sheets/time_sheets.dart';
import 'package:check_in/routes/routes.dart';
import 'package:get/get.dart';

import '../bindings/camera_binding.dart';
import '../bindings/auth/login_binding.dart';
import '../pages/auth/login_page.dart';
import '../pages/camera/camera_page.dart';
import '../pages/home/home_page.dart';

class Pages {
  static final pages = [
    GetPage(
      name: Routes.CAMERA_PAGE,
      page: () => CameraPage(),
      binding: CameraBindings(),
      transition: Transition.downToUp,
    ),
    GetPage(
      name: Routes.LOGIN_PAGE,
      page: () => const LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: Routes.HOMEPAGE,
      binding: HomeBindings(),
      page: () => HomePage(),
      transition: Transition.downToUp,
    ),
    GetPage(
      name: Routes.RESGISTER_PAGE,
      binding: RegisterBinding(),
      page: () => RegisterPage(),
    ),
    GetPage(
      name: Routes.FORGOT_PASSWORD_PAGE,
      binding: ForgotPasswordBindings(),
      page: () => const ForgotPasswordPage(),
    ),
    GetPage(
      name: Routes.EMAIL_SENT_PAGE,
      page: () => const EmailResetPasswordPage(),
    ),
    GetPage(
      name: Routes.SUCCESS_PAGE,
      page: () => const SuccessPage(),
    ),
    GetPage(
      name: Routes.SETTING_PAGE,
      page: () => const SettingPage(),
      transition: Transition.rightToLeft,
      binding: SettingBinding(),
    ),
    GetPage(
      name: Routes.DARK_MODE_PAGE,
      page: () => const DarkModePage(),
      transition: Transition.downToUp,
      binding: SettingBinding(),
    ),
    GetPage(
      name: Routes.LANGUAGE_PAGE,
      page: () => const LanguagePage(),
      transition: Transition.downToUp,
      binding: SettingBinding(),
    ),
    GetPage(
      name: Routes.PERSONAL_PAGE,
      page: () => const PersonalPage(),
      transition: Transition.rightToLeft,
      binding: PersonalBinding(),
    ),
    GetPage(
      name: Routes.CHANGE_PASSWORD_PAGE,
      page: () => const ChangePasswordPage(),
      transition: Transition.downToUp,
      binding: SettingBinding(),
    ),
    GetPage(
      name: Routes.TIME_SHEETS,
      page: () => const TimeSheets(),
      transition: Transition.downToUp,
      binding: TimeSheetsBinding(),
    ),
    GetPage(
      name: Routes.ABSENT,
      page: () => const AbsentPage(),
      transition: Transition.downToUp,
      binding: AbsentBindings(),
    ),
  ];
}
