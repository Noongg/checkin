// ignore_for_file: constant_identifier_names

class Routes {
  static const CAMERA_PAGE = "/camera";
  static const LOGIN_PAGE = "/login";
  static const RESGISTER_PAGE = "/register";
  static const FORGOT_PASSWORD_PAGE = "/forgot-password";
  static const EMAIL_SENT_PAGE = "/email-sent";
  static const ADD_AVATAR_PAGE = "/add-avatar";
  static const SUCCESS_PAGE = "/success";
  static const HOMEPAGE = "/homepage";
  static const SETTING_PAGE = "/setting";
  static const TIME_SHEETS = "/time-sheets";
  static const ABSENT = "/absent";
  static const DARK_MODE_PAGE = "/dark-mode";
  static const LANGUAGE_PAGE = "/language-page";
  static const PERSONAL_PAGE = "/personal-page";
  static const CHANGE_PASSWORD_PAGE = "/change-password-page";
}
